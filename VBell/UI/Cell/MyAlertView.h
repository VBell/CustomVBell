//
//  MyAlertView.h
//  PanasonicHEMS
//
//  Created by KW on 13/5/31.
//  Copyright (c) 2013年 mobile. All rights reserved.
//

#ifndef _MyAlertView_h_
#define _MyAlertView_h_

#import <UIKit/UIKit.h>

@protocol MyAlertViewProtocol <NSObject>
-(void)close;
@end

@interface MyAlertView : UIView
{
    __unsafe_unretained UIView* parentView;
    CGPoint lastTouchLocation;
    CGRect originalFrame;
    BOOL isShown;
    UIView* backgroundView;
    UIView* contentView;
}
@property (nonatomic) BOOL isShown;
- (id)initWithParent:(UIView*)parentV;
- (void)show;
- (void)hide;
@end

#endif
