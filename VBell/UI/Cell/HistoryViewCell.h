//
//  HistoryViewCell.h
//
//  Created by APPLE on 16/6/22.
//  Copyright (c) 2016年 Avadesign. All rights reserved.
//

#ifndef _HistoryViewCell_h_
#define _HistoryViewCell_h_

#import <UIKit/UIKit.h>

typedef enum DeviceWindowSize{
    DeviceWindowSize_6 = 0,
    DeviceWindowSize_5,
    DeviceWindowSize_4
} DeviceWindowSize;

@interface HistoryViewCell : UITableViewCell {
}
@property (retain, nonatomic) IBOutlet UILabel* timeForDay;
@property (retain, nonatomic) IBOutlet UILabel* deviceName;
@property (retain, nonatomic) IBOutlet UILabel* eventDescription;
@property (retain, nonatomic) IBOutlet UIView* eventDetailView;
@property (retain, nonatomic) IBOutlet UIImageView* cameraImage;

- (void)initWithDictionary:(NSDictionary*)detailData size:(DeviceWindowSize)_size;
@end

#endif
