//
//  listCell.m
//  idoubs
//
//  Created by APPLE on 12/9/28.
//  Copyright (c) 2012年 Doubango Telecom. All rights reserved.
//

#import "listCell.h"

@implementation listCell
@synthesize lab_mac;
@synthesize label_name;


- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        // Initialization code
    }
    return self;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

+(CGFloat)height
{
	return 185;
}

- (void)dealloc {
    [label_name release];
    [lab_mac release];
    [_button release];
    [super dealloc];
}
@end
