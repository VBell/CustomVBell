//
//  ContentView.m
//  linphone
//
//  Created by APPLE on 2014/10/30.
//
//

#import "ContentView.h"

@implementation ContentView
@synthesize tap;

- (id) initContentView
{
    self = [[[NSBundle mainBundle] loadNibNamed:@"ContentView" owner:self options:nil] lastObject];
    
    tap=[[UITapGestureRecognizer alloc] init];
    tap.numberOfTapsRequired=1;
    tap.numberOfTouchesRequired=1;
    
    [self addGestureRecognizer:tap];
    
    return self;
}

- (void)SetNotDP
{
    [_name setHidden:YES];
    
    [_mac setHidden:YES];
    
    [_setting setHidden:YES];
    
    [_image setHidden:YES];
    
    [_title setHidden:NO];
}

- (void)dealloc
{
    [_image release];
    [_name release];
    [_mac release];
    [_setting release];
    [_title release];
    [tap release];
    [super dealloc];
}
@end
