//
//  ContentView.h
//  linphone
//
//  Created by APPLE on 2014/10/30.
//
//

#ifndef _ContentView_h_
#define _ContentView_h_

#import <UIKit/UIKit.h>

@interface ContentView : UIView
{
    UITapGestureRecognizer *tap;
}
@property (retain, nonatomic) UITapGestureRecognizer *tap;


@property (retain, nonatomic) IBOutlet UIImageView *image;
@property (retain, nonatomic) IBOutlet UILabel *name;
@property (retain, nonatomic) IBOutlet UILabel *mac;
@property (retain, nonatomic) IBOutlet UIButton *setting;

@property (retain, nonatomic) IBOutlet UILabel *title;

- (id) initContentView;

- (void)SetNotDP;
@end

#endif
