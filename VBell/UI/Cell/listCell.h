//
//  listCell.h
//  idoubs
//
//  Created by APPLE on 12/9/28.
//  Copyright (c) 2012年 Doubango Telecom. All rights reserved.
//

#ifndef _listCell_h_
#define _listCell_h_

#import <UIKit/UIKit.h>

@interface listCell : UITableViewCell

@property (retain, nonatomic) IBOutlet UILabel *label_name;

+(CGFloat)height;

@property (retain, nonatomic) IBOutlet UILabel *lab_mac;
@property (retain, nonatomic) IBOutlet UIButton *button;
@end

#endif
