//
//  HelpView.h
//  linphone
//
//  Created by APPLE on 2014/10/31.
//
//

#ifndef _HelpView_h_
#define _HelpView_h_

#import <UIKit/UIKit.h>

@interface HelpView : UIView
{
    UIView *pView;
}
@property (retain, nonatomic) IBOutlet UIImageView *image1;
@property (retain, nonatomic) IBOutlet UIButton *button;
- (void)drawSubview:(id)parentView;
@end

#endif
