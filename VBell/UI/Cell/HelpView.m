//
//  HelpView.m
//  linphone
//
//  Created by APPLE on 2014/10/31.
//
//

#import "HelpView.h"
#import "DoorGuard_Util.h"

@implementation HelpView

#pragma mark Custom alert methods

- (void)drawSubview:(id)parentView;
{
    pView=parentView;
    
    if ([NSLocalizedString(LANG, nil) isEqualToString:@"zh"])
    {
        if (self.frame.size.height>=568)
            [_image1 setImage:[UIImage imageNamed:@"helpview4_zh.png"]];
        else
            [_image1 setImage:[UIImage imageNamed:@"helpview_zh.png"]];
        
        [_button setTitle:@"我知道了" forState:UIControlStateNormal];
    }
    else
    {
        if (self.frame.size.height>=568)
            [_image1 setImage:[UIImage imageNamed:@"helpview4_en.png"]];
        else
            [_image1 setImage:[UIImage imageNamed:@"helpview_en.png"]];
        
        [_button setTitle:@"I Know" forState:UIControlStateNormal];
    }
}

- (IBAction)CloseView:(id)sender
{
    [pView performSelector:@selector(hide) withObject:nil];
}

- (void)dealloc {
    [_image1 release];

    [_button release];
    [super dealloc];
}
@end
