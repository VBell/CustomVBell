//
//  HistoryViewCell.m
//
//  Created by APPLE on 16/6/22.
//  Copyright (c) 2016年 Avadesign. All rights reserved.
//

#import "HistoryViewCell.h"
#import "DoorGuard_Util.h"

@implementation HistoryViewCell
- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        // Initialization code
    }
    return self;
}

- (void)initWithDictionary:(NSDictionary*)detailData size:(DeviceWindowSize)_size
{
    NSString* deviceMAC = [detailData objectForKey:@"mac"];
    NSString* deviceName = [detailData objectForKey:@"name"];
    if ([deviceName length] > 0) {
        [self.deviceName setText:deviceName];
    } else {
        [self.deviceName setText:deviceMAC];
    }
    NSString* eventType = [detailData objectForKey:@"type"];
    if ([eventType isEqualToString:@"CALL"]) {
        [self.eventDescription setText:NSLocalizedString(HistoryViewCell_DetailInfo_CALL, nil)];
        [self.eventDetailView setBackgroundColor:[UIColor colorWithRed:155.0f/255.0f green:255.0f/255.0f blue:114.0f/255.0f alpha:1.0]];
    } else if ([eventType isEqualToString:@"ANSWER"]) {
        [self.eventDescription setText:NSLocalizedString(HistoryViewCell_DetailInfo_ANSWER, nil)];
        [self.eventDetailView setBackgroundColor:[UIColor colorWithRed:65.0f/255.0f green:117.0f/255.0f blue:5.0f/255.0f alpha:1.0]];
    } else if ([eventType isEqualToString:@"MOTION"]) {
        [self.eventDescription setText:NSLocalizedString(HistoryViewCell_DetailInfo_MOTION, nil)];
        [self.eventDetailView setBackgroundColor:[UIColor colorWithRed:245.0f/255.0f green:166.0f/255.0f blue:35.0f/255.0f alpha:1.0]];
    }
    [self.timeForDay setText:[detailData objectForKey:@"Time"]];
    NSString* snaphshotID = [detailData objectForKey:@"key"];
    if (snaphshotID && [snaphshotID length] > 0) {
        [self.cameraImage setHidden:NO];
    } else
        [self.cameraImage setHidden:YES];
    
    switch (_size) {
        case DeviceWindowSize_4:
            [self.deviceName setFont:[UIFont fontWithName:[[self.deviceName font] fontName] size:28]];
            break;
        case DeviceWindowSize_5:
            [self.deviceName setFont:[UIFont fontWithName:[[self.deviceName font] fontName] size:29]];
            break;
        case DeviceWindowSize_6:
            [self.deviceName setFont:[UIFont fontWithName:[[self.deviceName font] fontName] size:30]];
            break;
        default:
            break;
    }
}

- (void)dealloc {
    [self.timeForDay release];
    [self.deviceName release];
    [self.eventDescription release];
    [self.eventDetailView release];
    [self.cameraImage release];
    [super dealloc];
}
@end
