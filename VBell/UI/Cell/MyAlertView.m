//
//  MyAlertView.m
//  PanasonicHEMS
//
//  Created by KW on 13/5/31.
//  Copyright (c) 2013年 mobile. All rights reserved.
//

#import "MyAlertView.h"

#import "HelpView.h"


@implementation MyAlertView

@synthesize isShown;

- (id)initWithParent:(UIView*)parentV
{
    parentView = parentV;
    
    self = [super initWithFrame:CGRectMake(0, 0, parentView.frame.size.width, parentView.frame.size.height)];
    
    if (self)
    {
        self.autoresizesSubviews = YES;
        backgroundView = [[UIView alloc] initWithFrame:self.frame];
        backgroundView.backgroundColor = [UIColor clearColor];
        backgroundView.alpha = 0.5;
        
        [backgroundView setAutoresizingMask:UIViewAutoresizingFlexibleWidth|UIViewAutoresizingFlexibleHeight];
        
        [self addSubview:backgroundView];
        
        contentView = [self addContentView];
        
        [self addSubview:contentView];
        
        originalFrame = contentView.frame;
        
        self.alpha = 0;
        
        self.backgroundColor = [UIColor clearColor];
        
        [parentView addSubview:self];
    }
    return self;
}

- (void)closePopView
{
    
}

- (id)addContentView
{
    HelpView* hv = [[[NSBundle mainBundle] loadNibNamed:@"HelpView" owner:self options:nil] lastObject];
    
    hv.frame=self.frame;
    
    
    NSLog(@"%@",NSStringFromCGRect(self.frame));
    NSLog(@"%@",NSStringFromCGRect(hv.frame));
    
    
    
    [hv drawSubview:self];
    
    return hv;
}

#pragma mark Custom alert methods

- (void)show
{
//    NSLog(@"show");
    isShown = YES;
    contentView.transform = CGAffineTransformMakeScale(0.1, 0.1);
    self.alpha = 0;
    [UIView beginAnimations:@"showAlert" context:nil];
    [UIView setAnimationDelegate:self];
    contentView.transform = CGAffineTransformMakeScale(1.1, 1.1);
    self.alpha = 1;
    [UIView commitAnimations];
}

- (void)hide
{
//    NSLog(@"hide");
    isShown = NO;
    [UIView beginAnimations:@"hideAlert" context:nil];
    [UIView setAnimationDelegate:self];
    contentView.transform = CGAffineTransformMakeScale(0.1, 0.1);
    self.alpha = 0;
    [UIView commitAnimations];
    
}

- (void)toggle
{
    if (isShown) {
        [self hide];
    } else {
        [self show];
    }
}

#pragma mark Animation delegate

- (void)animationDidStop:(NSString *)animationID finished:(NSNumber *)finished context:(void *)context
{
    if ([animationID isEqualToString:@"showAlert"]) {
        if (finished) {
            [UIView beginAnimations:nil context:nil];
            contentView.transform = CGAffineTransformMakeScale(1.0, 1.0);
            [UIView commitAnimations];
        }
    } else if ([animationID isEqualToString:@"hideAlert"]) {
        if (finished) {
            contentView.transform = CGAffineTransformMakeScale(1.0, 1.0);
            contentView.frame = originalFrame;
            //[self removeFromSuperview];
        }
    }
}

#pragma mark Touch methods
/*
- (void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event
{
    UITouch *touch = [touches anyObject];
    lastTouchLocation = [touch locationInView:self];
}

- (void)touchesMoved:(NSSet *)touches withEvent:(UIEvent *)event
{
    UITouch *touch = [touches anyObject];
    CGPoint newTouchLocation = [touch locationInView:self];
    CGRect currentFrame = self.frame;
    
    CGFloat deltaX = lastTouchLocation.x - newTouchLocation.x;
    CGFloat deltaY = lastTouchLocation.y - newTouchLocation.y;
    
    self.frame = CGRectMake(currentFrame.origin.x - deltaX, currentFrame.origin.y - deltaY, currentFrame.size.width, currentFrame.size.height);
    lastTouchLocation = [touch locationInView:self];
}

- (void)touchesEnded:(NSSet *)touches withEvent:(UIEvent *)event
{
    
}

- (void)touchesCancelled:(NSSet *)touches withEvent:(UIEvent *)event
{
    
}
*/

@end
