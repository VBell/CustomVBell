//
//  AddCamViewController.h
//  idoubs
//
//  Created by APPLE on 12/12/5.
//  Copyright (c) 2012年 Doubango Telecom. All rights reserved.
//

#ifndef _AddCamViewController_h_
#define _AddCamViewController_h_

#import <UIKit/UIKit.h>
#import "ZBarSDK.h"
#import <VBellCloud/VBellCloud.h>

@interface AddCamViewController : UIViewController<UITextFieldDelegate,ZBarReaderDelegate,ZBarReaderViewDelegate,UIAlertViewDelegate,NSURLConnectionDelegate>
{
    ZBarReaderView *readerView;
    ZBarCameraSimulator *cameraSim;
    BOOL QR_Flag;
    NSMutableData *receivedata;
    CGRect screenBounds;
}
@property (retain, nonatomic) IBOutlet ZBarReaderView *readerView;
-(IBAction)button_down:(id)sender;
-(IBAction)button_Bar_down:(id)sender;
@property (retain, nonatomic) IBOutlet UITextField *text_activation;
@property (retain, nonatomic) IBOutlet UILabel *lab_title;
@property (retain, nonatomic) IBOutlet UILabel *lab_info1;
@property (retain, nonatomic) IBOutlet UILabel *lab_info2;
@property (retain, nonatomic) IBOutlet UIButton *button_back;
@property (retain, nonatomic) IBOutlet UIButton *button_qr;
@property (retain, nonatomic) IBOutlet UIButton *button_submit;
@property (retain, nonatomic) IBOutlet UIButton *button_cancel;
@end

#endif
