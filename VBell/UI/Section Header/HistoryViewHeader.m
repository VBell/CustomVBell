//
//  HistoryViewHeader.m
//
//  Created by APPLE on 16/6/22.
//  Copyright (c) 2016年 Avadesign. All rights reserved.
//

#import "HistoryViewHeader.h"

@implementation HistoryViewHeader
- (instancetype)initWithReuseIdentifier:(nullable NSString *)reuseIdentifier
{
    self = [super initWithReuseIdentifier:reuseIdentifier];
    if (self) {
        // Initialization code
    }
    return self;
}
- (void)initWithDateString:(NSString*)_date
{
    [self.dateLabel setText:_date];
}
- (void)dealloc {
    [self.dateLabel release];
    [super dealloc];
}
@end
