//
//  HistoryViewHeader.h
//
//  Created by APPLE on 16/6/22.
//  Copyright (c) 2016年 Avadesign. All rights reserved.
//

#ifndef _HistoryViewHeader_h_
#define _HistoryViewHeader_h_

#import <UIKit/UIKit.h>
@interface HistoryViewHeader : UITableViewHeaderFooterView {
}
@property (retain, nonatomic) IBOutlet UILabel* dateLabel;
- (void)initWithDateString:(NSString*)_date;
@end

#endif
