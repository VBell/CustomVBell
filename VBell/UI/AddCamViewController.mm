//
//  AddCamViewController.m
//  idoubs
//
//  Created by APPLE on 12/12/5.
//  Copyright (c) 2012年 Doubango Telecom. All rights reserved.
//

#import "AddCamViewController.h"
#import <QuartzCore/QuartzCore.h>
#import "LinphoneAppDelegate.h"

#pragma mark - AddCamViewController (Http)

@interface AddCamViewController (Http)
-(void) keyboardWillHide:(NSNotification *)note;
-(void) keyboardWillShow:(NSNotification *)note;
-(void) keyboardNotificationWithNote:(NSNotification *)note willShow: (BOOL) showing;
- (void)onAddDeviceFinish:(NSNotification*)notif;
- (void)ResponseHandlerInit;
@end

@implementation AddCamViewController (Http)

#pragma mark -KeyBrard

-(void) keyboardWillHide:(NSNotification *)note
{
	[self keyboardNotificationWithNote:note willShow:NO];
}

-(void) keyboardWillShow:(NSNotification *)note
{
	[self keyboardNotificationWithNote:note willShow:YES];
}

-(void) keyboardNotificationWithNote:(NSNotification *)note willShow:(BOOL)showing
{
    [UIView beginAnimations:nil context:NULL];
    [UIView setAnimationDuration:0.5f];
    int i = 0;
    if (showing) {
        if (screenBounds.size.height >= 568)
            i = -60;
        else
            i = -150;
    } else
        i = 0;
    CGRect frame;
    frame = self.view.frame;
    frame.origin.y = i;
    self.view.frame = frame;
    [UIView commitAnimations];
}

- (void)onAddDeviceFinish:(NSNotification*)notif
{
    NSDictionary* resp = [notif userInfo];
    NSString* res_code = [resp objectForKey:@"res_code"];
    NSString* status = [resp objectForKey:@"status"];
    if ([res_code isEqualToString:@"200"]) {
        if ([status isEqualToString:@"C_ERx0000"]) {
            UIAlertView *alert=[[UIAlertView alloc]initWithTitle:NSLocalizedString(SUCCESS, nil) message:[NSString stringWithFormat:@"%@%@",NSLocalizedString(ADD, nil),NSLocalizedString(SUCCESS, nil)] delegate:nil cancelButtonTitle:NSLocalizedString(OK, nil) otherButtonTitles:nil, nil];
            [alert show];
            [alert release];
            [self.navigationController popViewControllerAnimated:YES];
        } else {
            UIAlertView *alert=[[UIAlertView alloc] initWithTitle:[resp objectForKey:@"title"] message:[resp objectForKey:@"detail"] delegate:nil cancelButtonTitle:nil otherButtonTitles:NSLocalizedString(OK, nil), nil];
            [alert show];
            [alert release];
        }
        [[LinphoneAppDelegate sharedInstance] HideBusyView];
    } else {
        UIAlertView *alert=[[UIAlertView alloc] initWithTitle:NSLocalizedString(CON_ERROR_TITLE, nil) message:NSLocalizedString(CON_ERROR_MESSAGE, nil) delegate:nil cancelButtonTitle:nil otherButtonTitles:NSLocalizedString(OK, nil), nil];
        [alert show];
        [alert release];
        [[LinphoneAppDelegate sharedInstance] HideBusyView];
    }
}

- (void)ResponseHandlerInit
{
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(onAddDeviceFinish:) name:kAddDeviceResponse object:nil];
}
@end

#pragma mark - AddCamViewController

@implementation AddCamViewController
@synthesize text_activation;
@synthesize button_back;
@synthesize lab_title;
@synthesize lab_info1;
@synthesize lab_info2;
@synthesize button_qr;
@synthesize button_submit;
@synthesize button_cancel;
@synthesize readerView;

#pragma mark - init

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (UIStatusBarStyle)preferredStatusBarStyle
{
    return UIStatusBarStyleLightContent;
}

- (BOOL)prefersStatusBarHidden
{
    return NO;
}

#pragma mark - viewload

- (void)viewDidLoad
{
    [super viewDidLoad];
    [self ResponseHandlerInit];
    screenBounds = [[UIScreen mainScreen] bounds];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardWillShow:) name:UIKeyboardWillShowNotification object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardWillHide:) name:UIKeyboardWillHideNotification object:nil];
    lab_title.text=NSLocalizedString(ADDDP, nil);
    lab_info1.text=NSLocalizedString(SCAN_OR_INPUT, nil);
    [button_back setTitle:NSLocalizedString(BACK, nil) forState:UIControlStateNormal];
    [button_qr setTitle:NSLocalizedString(SCAN, nil) forState:UIControlStateNormal];
    [button_submit setTitle:NSLocalizedString(SUBMIT, nil) forState:UIControlStateNormal];
    [button_cancel setTitle:NSLocalizedString(CANCEL, nil) forState:UIControlStateNormal];
}

#pragma mark - ZBarReaderDelegate

- (void) readerView: (ZBarReaderView*)view didReadSymbols:(ZBarSymbolSet*)syms fromImage: (UIImage*)img
{
    for(ZBarSymbol *sym in syms)
    {
        [NSObject cancelPreviousPerformRequestsWithTarget:self selector:@selector(read_down) object:nil];
        text_activation.text=sym.data;
        QR_Flag=NO;
        break;
    }
    [self performSelector:@selector(read_down) withObject:nil afterDelay:1.5];
}

-(void)read_down
{
    [readerView stop];
    [readerView setHidden:YES];
}

#pragma mark - button

-(IBAction)button_Bar_down:(id)sender
{
    if (QR_Flag)
    {
        [readerView stop];
        [readerView setHidden:YES];
        QR_Flag=NO;
    }
    else
        [self.navigationController popViewControllerAnimated:YES];
}

-(IBAction)button_down:(id)sender
{
    UIButton* button = sender;
    [text_activation resignFirstResponder];
    if (button.tag == 0)
        [[LinphoneAppDelegate sharedInstance].vbellCloud addDeviceWithCode:text_activation.text withTag:0 returnToMainThread:YES];
    else if (button.tag == 1)
    {
        QR_Flag = YES;
        [ZBarReaderView class];
        readerView.readerDelegate = self;
        if (cameraSim != nil)
            [cameraSim release];
        cameraSim = [[ZBarCameraSimulator alloc] initWithViewController: self];
        cameraSim.readerView = readerView;
        [readerView setHidden:NO];
        [readerView start];
    }
    else if (button.tag == 3)
        text_activation.text = @"";
}

#pragma mark - touch

- (void)touchesEnded:(NSSet *)touches withEvent:(UIEvent *)event
{
    if (![text_activation isExclusiveTouch])
        [text_activation resignFirstResponder];
}

#pragma mark - UITextFieldDelegate

-(BOOL)textFieldShouldReturn:(UITextField *)textField
{
    [textField resignFirstResponder];
    return YES;
}

#pragma mark - rotate

//for ios <=5
- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    return (interfaceOrientation==UIInterfaceOrientationPortrait);
}

- (void)dealloc
{
    [readerView release];
    [cameraSim release];
    [[NSNotificationCenter defaultCenter] removeObserver: self];
    [text_activation release];
    [button_back release];
    [lab_title release];
    [lab_info1 release];
    [button_qr release];
    [button_submit release];
    [button_cancel release];
    [lab_info2 release];
    [super dealloc];
}
- (void)viewDidUnload
{
    [self setReaderView:nil];
    cameraSim = nil;
    [self setText_activation:nil];
    [self setButton_back:nil];
    [self setButton_qr:nil];
    [self setButton_submit:nil];
    [self setButton_cancel:nil];
    [self setLab_info2:nil];
    [super viewDidUnload];
}
@end
