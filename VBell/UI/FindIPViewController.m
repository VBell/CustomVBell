//
//  FindIPViewController.m
//  CustomVBell
//
//  Created by APPLE on 2016/7/5.
//
//

#import "FindIPViewController.h"

@interface FindIPViewController (Private)
- (void)startFinding;
@end

@implementation FindIPViewController (Private)
- (void)startFinding
{
    [self.loadingIndicator startAnimating];
    [self.deviceMAC setText:find_target];
    [self.ipAddress setText:@"  "];
    sdp = [[ScanDP alloc] initScanDPWithDelegate:self];
    [sdp StartScan];
}
@end

@implementation FindIPViewController

- (UIStatusBarStyle)preferredStatusBarStyle
{
    return UIStatusBarStyleLightContent;
}

- (BOOL)prefersStatusBarHidden
{
    return YES;
}

#pragma mark - View Init

- (void)viewDidLoad
{
    [super viewDidLoad];
    [self.topLabel setText:NSLocalizedString(FIND_IP, nil)];
    [self.backButton setTitle:NSLocalizedString(BACK, nil) forState:UIControlStateNormal];
    [self.deviceLabel setText:NSLocalizedString(FindIPViewController_DeviceLabel, nil)];
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    [self startFinding];
}

- (IBAction)back_down:(id)sender
{
    [self.navigationController popViewControllerAnimated:YES];
}

- (void)dealloc {
    [self.topLabel release];
    [self.backButton release];
    [self.deviceLabel release];
    [self.deviceMAC release];
    [self.ipLabel release];
    [self.ipAddress release];
    [self.loadingIndicator release];
    [super dealloc];
}

- (void)setFindedDevice:(NSString*)_device_mac
{
    find_target = [[NSString alloc] initWithString:_device_mac];
}

- (void)ScanDP_Result:(NSMutableArray *)cam_array
{
    [self.loadingIndicator stopAnimating];
    NSMutableArray<NSMutableDictionary*>* tmp_array = cam_array;
    for (NSInteger findedIndex = 0; findedIndex < [tmp_array count]; findedIndex++) {
        if ([[[[tmp_array objectAtIndex:findedIndex] objectForKey:@"MAC"] uppercaseString] isEqualToString:[self.deviceMAC.text uppercaseString]]) {
            [self.ipAddress setText:[[tmp_array objectAtIndex:findedIndex] objectForKey:@"IP"]];
            return;
        }
    }
    [self.ipAddress setText:NSLocalizedString(FindIPViewController_NotFound, nil)];
}

@end

