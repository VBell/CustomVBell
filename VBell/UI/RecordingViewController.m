//
//  RecordingViewController.m
//  CustomVBell
//
//  Created by APPLE on 2016/7/5.
//
//

#import "RecordingViewController.h"
#import "LinphoneAppDelegate.h"

@interface RecordingViewController (Private)
- (void)startFinding;
@end

@implementation RecordingViewController (Private)
- (void)startFinding
{
    [self.loadingIndicator startAnimating];
    [self.recordingPage setHidden:YES];
    sdp=[[ScanDP alloc] initScanDPWithDelegate:self];
    [sdp StartScan];
}
@end

@implementation RecordingViewController

- (UIStatusBarStyle)preferredStatusBarStyle
{
    return UIStatusBarStyleLightContent;
}

- (BOOL)prefersStatusBarHidden
{
    return YES;
}

#pragma mark - View Init

- (void)viewDidLoad
{
    [super viewDidLoad];
    [self.topLabel setText:NSLocalizedString(RECORDING, nil)];
    [self.backButton setTitle:NSLocalizedString(BACK, nil) forState:UIControlStateNormal];
    [self.loadingLabel setText:NSLocalizedString(RecordingViewController_LoadingLabel, nil)];
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    [self startFinding];
}

- (IBAction)back_down:(id)sender
{
    [self.navigationController popViewControllerAnimated:YES];
}

- (void)dealloc {
    [self.topLabel release];
    [self.backButton release];
    [self.loadingIndicator release];
    [self.loadingLabel release];
    [self.loadingIndicator release];
    [self.recordingPage release];
    [super dealloc];
}

- (void)setFindedDevice:(NSString*)_device_mac
{
    find_target = [[NSString alloc] initWithString:_device_mac];
}

- (void)ScanDP_Result:(NSMutableArray *)cam_array
{
    [self.loadingIndicator stopAnimating];
    [self.recordingPage setHidden:NO];
    NSMutableArray<NSMutableDictionary*>* tmp_array = cam_array;
    for (NSInteger findedIndex = 0; findedIndex < [tmp_array count]; findedIndex++) {
        if ([[[[tmp_array objectAtIndex:findedIndex] objectForKey:@"MAC"] uppercaseString] isEqualToString:[find_target uppercaseString]]) {
            NSString* url_str = [[NSString alloc] initWithFormat:@"http://%@/DP107/Record.ncgi", [[tmp_array objectAtIndex:findedIndex] objectForKey:@"IP"]];
            NSURLRequest* requestObj = [NSURLRequest requestWithURL:[NSURL URLWithString:url_str]];
            [self.recordingPage loadRequest:requestObj];
            return;
        }
    }
}

@end

