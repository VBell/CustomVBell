//
//  WiFiSettingController.m
//  linphone
//
//  Created by APPLE on 2014/11/6.
//
//

#import "WiFiSettingController.h"
#import "DoorGuard_Util.h"
#import <SystemConfiguration/CaptiveNetwork.h>
#import "LinphoneAppDelegate.h"

@interface WiFiSettingController (Private)
- (void)onSetNetworkFinish:(NSNotification*)notif;
- (void)onGetNetworkFinish:(NSNotification*)notif;

- (void)SetScrollView:(NSDictionary *)item;
- (void)ResponseHandlerInit;
@end

@implementation WiFiSettingController (Private)
- (void)onSetNetworkFinish:(NSNotification*)notif
{
    NSDictionary* resp = [notif userInfo];
    NSString* res_code = [resp objectForKey:@"res_code"];
    NSString* status = [resp objectForKey:@"status"];
    if ([res_code isEqualToString:@"200"]) {
        if (status && [status isEqualToString:@"200 ok"]) {
            CGFloat width, height;
            width = self.scroll.frame.size.width;
            height = self.scroll.frame.size.height;
            CGRect frame = CGRectMake(width*nowPage, 0, width, height);
            [self.scroll scrollRectToVisible:frame animated:YES];
        } else {
            nowPage-=1;
            UIAlertView *alert=[[UIAlertView alloc] initWithTitle:NSLocalizedString(CON_ERROR_TITLE, nil) message:NSLocalizedString(CON_ERROR_MESSAGE, nil) delegate:nil cancelButtonTitle:nil otherButtonTitles:NSLocalizedString(OK, nil), nil];
            [alert show];
            [alert release];
        }
    } else {
        nowPage-=1;
        UIAlertView *alert=[[UIAlertView alloc] initWithTitle:NSLocalizedString(CON_ERROR_TITLE, nil) message:NSLocalizedString(CON_ERROR_MESSAGE, nil) delegate:nil cancelButtonTitle:nil otherButtonTitles:NSLocalizedString(OK, nil), nil];
        [alert show];
        [alert release];
    }
    [[LinphoneAppDelegate sharedInstance] HideBusyView];
}

- (void)onGetNetworkFinish:(NSNotification*)notif
{
    NSDictionary* resp = [notif userInfo];
    NSString* res_code = [resp objectForKey:@"res_code"];
    NSString* status = [resp objectForKey:@"status"];
    if ([res_code isEqualToString:@"200"]) {
        if ([status isEqualToString:@"200"]) {
            [self SetScrollView:[resp objectForKey:@"network"]];
        } else
            [self SetScrollView:nil];
    } else {
        nowPage-=1;
        UIAlertView *alert=[[UIAlertView alloc] initWithTitle:NSLocalizedString(CON_ERROR_TITLE, nil) message:NSLocalizedString(CON_ERROR_MESSAGE, nil) delegate:nil cancelButtonTitle:nil otherButtonTitles:NSLocalizedString(OK, nil), nil];
        [alert show];
        [alert release];
    }
    [[LinphoneAppDelegate sharedInstance] HideBusyView];
}

- (void)SetScrollView:(NSDictionary *)item
{
    [self.scroll setContentSize:CGSizeMake(self.scroll.frame.size.width*3, self.scroll.frame.size.height)];
    [self.view1 setFrame:CGRectMake(self.scroll.frame.size.width*0, 0, self.scroll.frame.size.width, self.scroll.frame.size.height)];
    [self.view2 setFrame:CGRectMake(self.scroll.frame.size.width*1, 0, self.scroll.frame.size.width, self.scroll.frame.size.height)];
    [self.view3 setFrame:CGRectMake(self.scroll.frame.size.width*2, 0, self.scroll.frame.size.width, self.scroll.frame.size.height)];
    nowPage=0;
    nowtext=0;
    if (item==nil)
    {
        self.text1.text=[UserInfo getStringForKey:@"SSID"];
        self.text2.text=@"";
        self.text3.text=@"WPA2PSK";
        self.text4.text=@"AES";
    }
    else
    {
        NSString *ssid=[item objectForKey:@"SSID"];
        NSString *auth=[item objectForKey:@"AUTH_MODE"];
        NSString *encry=[item objectForKey:@"ENCRYPT_TYPE"];
        NSString *key=[item objectForKey:@"AUTH_KEY"];
        if ([ssid isEqualToString:@""])
            self.text1.text=[UserInfo getStringForKey:@"SSID"];
        else
            self.text1.text=ssid;
        self.text2.text=key;
        if ([auth isEqualToString:@""])
            self.text3.text=[UserInfo getStringForKey:@"AUTH_MODE"];
        else
            self.text3.text=auth;
        if ([encry isEqualToString:@""])
            self.text4.text=[UserInfo getStringForKey:@"ENCRYPT_TYPE"];
        else
            self.text4.text=encry;
    }
    [self.scroll addSubview:self.view1];
    [self.scroll addSubview:self.view2];
    [self.scroll addSubview:self.view3];
    if (Find)
    {
        nowPage=1;
        CGFloat width, height;
        width = self.scroll.frame.size.width;
        height = self.scroll.frame.size.height;
        CGRect frame = CGRectMake(width*nowPage, 0, width, height);
        [self.scroll scrollRectToVisible:frame animated:YES];
    }
    else
        nowPage=0;
}

- (void)ResponseHandlerInit
{
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(onSetNetworkFinish:) name:kSetNetworkResponse object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(onGetNetworkFinish:) name:kGetNetworkResponse object:nil];
}
@end

@interface WiFiSettingController ()

@end

@implementation WiFiSettingController
@synthesize mac;
@synthesize model;

- (UIStatusBarStyle)preferredStatusBarStyle
{
    return UIStatusBarStyleLightContent;
}

- (BOOL)prefersStatusBarHidden
{
    if ([[UserInfo platformString] hasPrefix:@"iPad"])
        return YES;
    return NO;
}

#pragma mark - View Init

- (void)viewDidLoad
{
    [super viewDidLoad];
    [self ResponseHandlerInit];
    NSString *mac_fix=@"";
    if ([[model uppercaseString] isEqualToString:@"DP104W"])
    {
        mac_fix=[NSString stringWithFormat:@"DP104W_xx-%@-%@-%@-%@-%@",[[mac lowercaseString] substringWithRange:NSMakeRange(2, 2)],[[mac lowercaseString] substringWithRange:NSMakeRange(4, 2)],[[mac lowercaseString] substringWithRange:NSMakeRange(6, 2)],[[mac lowercaseString] substringWithRange:NSMakeRange(8, 2)],[[mac lowercaseString] substringWithRange:NSMakeRange(10, 2)]];
    }
    else if ([[model uppercaseString] isEqualToString:@"DP105"])
    {
        mac_fix=[NSString stringWithFormat:@"DP105_xx-%@-%@-%@-%@-%@",[[mac lowercaseString] substringWithRange:NSMakeRange(2, 2)],[[mac lowercaseString] substringWithRange:NSMakeRange(4, 2)],[[mac lowercaseString] substringWithRange:NSMakeRange(6, 2)],[[mac lowercaseString] substringWithRange:NSMakeRange(8, 2)],[[mac lowercaseString] substringWithRange:NSMakeRange(10, 2)]];
    }
    else if ([[model uppercaseString] isEqualToString:@"DP106"])
    {
        mac_fix=[NSString stringWithFormat:@"DP106_xx-%@-%@-%@-%@-%@",[[mac lowercaseString] substringWithRange:NSMakeRange(2, 2)],[[mac lowercaseString] substringWithRange:NSMakeRange(4, 2)],[[mac lowercaseString] substringWithRange:NSMakeRange(6, 2)],[[mac lowercaseString] substringWithRange:NSMakeRange(8, 2)],[[mac lowercaseString] substringWithRange:NSMakeRange(10, 2)]];
    }
    else if ([[model uppercaseString] isEqualToString:@"DP107"])
    {
        mac_fix=[NSString stringWithFormat:@"DP107_xx-xx-xx-xx-xx-xx"];
    }
    _lab_info.text=[NSString stringWithFormat:NSLocalizedString(WIFI_INFO1, nil),mac_fix];
    _lab_info2.text=NSLocalizedString(WIFI_INFO2, nil);
    [_button_back setTitle:NSLocalizedString(BACK, nil) forState:UIControlStateNormal];
    [_button_next setTitle:NSLocalizedString(NEXT, nil) forState:UIControlStateNormal];
    [_button_done setTitle:NSLocalizedString(OK, nil) forState:UIControlStateNormal];
    _lab1.text=NSLocalizedString(WIFI_NAME, nil);
    _lab2.text=NSLocalizedString(WIFI_KEY, nil);
    _lab3.text=NSLocalizedString(WIFI_AUTH, nil);
    _lab4.text=NSLocalizedString(WIFI_ENCRY, nil);
    _lab_show.text=NSLocalizedString(SHOW, nil);
    picker_list=[[NSMutableArray alloc] init];
    [_ass_view setFrame:CGRectMake(0, 0, 320, 40)];
    _text3.inputView=_picker;
    _text4.inputView=_picker;
    _text3.inputAccessoryView=_ass_view;
    _text4.inputAccessoryView=_ass_view;
    scan_ip=[[NSString alloc] initWithString:@"192.168.100.1"];
}

- (void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
    Find=NO;
    [[LinphoneAppDelegate sharedInstance] ShowBusyViewWithMessage:NSLocalizedString(WAIT, nil)];
    sdp=[[ScanDP alloc] initScanDPWithDelegate:self];
    [sdp StartScan];
}

- (void)ScanDP_Result:(NSMutableArray *)cam_array
{
    for(NSDictionary *cam_dic in cam_array)
    {
        if ([[[cam_dic objectForKey:@"MAC"] lowercaseString] isEqualToString:[mac lowercaseString]])
        {
            Find=YES;
            if (scan_ip!=nil)
                [scan_ip release];
            scan_ip=[[NSString alloc] initWithString:[cam_dic objectForKey:@"IP"]];
            break;
        }
    }
    [sdp release];
    if (Find) {
        [[LinphoneAppDelegate sharedInstance].vbellCloud getNetworkForDeviceIP:scan_ip withModel:model WithTag:0 returnToMainThread:YES];
    } else {
        [self SetScrollView:nil];
        [[LinphoneAppDelegate sharedInstance] HideBusyView];
    }
}

- (IBAction)button_down:(id)sender
{
    if (sender==_button_next)
        nowPage+=1;
    else
        nowPage-=1;
    if (nowPage<Find ? 1 : 0 || nowPage>=3)
        [self.navigationController popViewControllerAnimated:YES];
    else if (nowPage==2)
    {
        if (_text1.text.length>1 && _text2.text.length>1 && _text3.text.length>1 && _text4.text.length>1) {
            [[LinphoneAppDelegate sharedInstance] ShowBusyViewWithMessage:NSLocalizedString(WAIT, nil)];
            [[LinphoneAppDelegate sharedInstance].vbellCloud setNetworkForDeviceIP:scan_ip withModel:model withSSID:self.text1.text withAmode:self.text3.text withEtype:self.text4.text withAkey:self.text2.text WithTag:0 returnToMainThread:YES];
        } else {
            nowPage-=1;
            UIAlertView *alert=[[UIAlertView alloc] initWithTitle:NSLocalizedString(ERROR_TITLE, nil) message:NSLocalizedString(EMPTY, nil) delegate:nil cancelButtonTitle:nil otherButtonTitles:NSLocalizedString(OK, nil), nil];
            [alert show];
            [alert release];
        }
    }
    else
    {
        [_button_next setTitle:NSLocalizedString(NEXT, nil) forState:UIControlStateNormal];
        CGFloat width, height;
        width = _scroll.frame.size.width;
        height = _scroll.frame.size.height;
        CGRect frame = CGRectMake(width*nowPage, 0, width, height);
        [_scroll scrollRectToVisible:frame animated:YES];
    }
}

#pragma mark -UITextFieldDelegate

- (BOOL)textFieldShouldBeginEditing:(UITextField *)textField
{
    if (textField==_text3)
    {
        [picker_list removeAllObjects];
        [picker_list addObject:@"OPEN"];
        [picker_list addObject:@"SHARED"];
        [picker_list addObject:@"WPAPSK"];
        [picker_list addObject:@"WPA2PSK"];
    }
    else if (textField==_text4)
    {
        if ([_text3.text isEqualToString:@"OPEN"])
        {
            [picker_list removeAllObjects];
            [picker_list addObject:@"NONE"];
        }
        else if ([_text3.text isEqualToString:@"SHARED"])
        {
            [picker_list removeAllObjects];
            [picker_list addObject:@"WEP"];
        }
        else
        {
            [picker_list removeAllObjects];
            [picker_list addObject:@"TKIP"];
            [picker_list addObject:@"AES"];
        }
    }
    [_picker reloadAllComponents];
    nowtext=textField.tag;
    return YES;
}

- (BOOL)textFieldShouldReturn:(UITextField *)textField
{
    [textField resignFirstResponder];
    return YES;
}

- (BOOL)textFieldShouldEndEditing:(UITextField *)textField
{
    return YES;
}

- (IBAction)done:(id)sender
{
    [_text1 resignFirstResponder];
    [_text2 resignFirstResponder];
    [_text3 resignFirstResponder];
    [_text4 resignFirstResponder];
    if (nowtext==2)
    {
        _text3.text=[picker_list objectAtIndex:[_picker selectedRowInComponent:0]];
        if ([_text3.text isEqualToString:@"OPEN"])
            _text4.text=@"NONE";
        else if ([_text3.text isEqualToString:@"SHARED"])
            _text4.text=@"WEP";
        else
            _text4.text=@"AES";
    }
    else if (nowtext==3)
        _text4.text=[picker_list objectAtIndex:[_picker selectedRowInComponent:0]];
}

#pragma mark - UIPickerViewDelegate

-(void)pickerView:(UIPickerView *)pickerView didSelectRow:(NSInteger)row inComponent:(NSInteger)component
{
}

- (NSInteger)numberOfComponentsInPickerView:(UIPickerView *)thePickerView
{
    return 1;
}

- (NSInteger)pickerView:(UIPickerView *)thePickerView numberOfRowsInComponent:(NSInteger)component
{
    return [picker_list count];
}

- (NSString *)pickerView:(UIPickerView *)thePickerView titleForRow:(NSInteger)row forComponent:(NSInteger)component
{
    return [picker_list objectAtIndex:row];
}

- (UIView *)pickerView:(UIPickerView *)pickerView viewForRow:(NSInteger)row forComponent:(NSInteger)component reusingView:(UIView *)view
{
    UILabel *myView = nil;
    myView = [[UILabel alloc] initWithFrame:CGRectMake(0.0, 0.0, 320, 30)];
    myView.textAlignment = NSTextAlignmentCenter;
    myView.text = [picker_list objectAtIndex:row];
    myView.font = [UIFont systemFontOfSize:16];
    myView.textColor=[UIColor blackColor];
    myView.backgroundColor = [UIColor clearColor];
    return myView;
}

- (IBAction)ad_set_down:(id)sender
{
    [UIView animateWithDuration:0.5 animations:^(void)
    {
        if (_layout.constant!=0)
            [_layout setConstant:0];
        else
            [_layout setConstant:-300];
        [self.view layoutIfNeeded];
    }];
}

- (IBAction)sw_down:(id)sender
{
    UISwitch *sw=sender;
    [_text2 setSecureTextEntry:!sw.isOn];
}

- (void)dealloc {
    [[NSNotificationCenter defaultCenter] removeObserver:self];
    [picker_list release];
    [mac release];
    [model release];
    [scan_ip release];
    [_lab_info release];
    [_button_next release];
    [_button_back release];
    [_view1 release];
    [_scroll release];
    [_view2 release];
    [_lab1 release];
    [_lab2 release];
    [_lab3 release];
    [_lab4 release];
    [_text1 release];
    [_text2 release];
    [_text3 release];
    [_text4 release];
    [_picker release];
    [_ass_view release];
    [_button_done release];
    [_layy release];
    [_view3 release];
    [_lab_info2 release];
    [_layout release];
    [_lab_show release];
    [_switch_show release];
    [super dealloc];
}
@end
