//
//  HomeViewController.h
//  linphone
//
//  Created by APPLE on 2014/7/14.
//
//

#ifndef _HomeViewController_h_
#define _HomeViewController_h_

#import <UIKit/UIKit.h>
#import <VBellCloud/VBellCloud.h>
#import "ContentView.h"
#import "SettingViewController.h"

@interface HomeViewController : UIViewController<UIActionSheetDelegate,UIAlertViewDelegate,UIScrollViewDelegate>
{
    NSMutableDictionary* camState;
    BOOL qr_show;
}
@property (retain, nonatomic) IBOutlet UILabel *title1;
@property (retain, nonatomic) IBOutlet UILabel *title2;
@property (retain, nonatomic) IBOutlet UIButton *button_adddp;
@property (retain, nonatomic) IBOutlet UIButton *button_setting;
@property (retain, nonatomic) IBOutlet UIButton* button_history;
@property (retain, nonatomic) IBOutlet UIScrollView *scroll1;
@property (retain, nonatomic) IBOutlet UIScrollView *scroll2;
@property (retain, nonatomic) IBOutlet UIPageControl *page1;
@property (retain, nonatomic) IBOutlet UIPageControl *page2;
@property (retain, nonatomic) IBOutlet UIView* tab_view;
- (void)RefreshScroll;
@end

#endif
