//
//  ViewerViewController.m
//  linphone
//
//  Created by APPLE on 2014/8/26.
//
//

#import "ViewerViewController.h"
#import "LinphoneAppDelegate.h"

@interface ViewerViewController (Private)
- (void)onAddViewerFinish:(NSNotification*)notif;
- (void)onRemoveViewerFinish:(NSNotification*)notif;
- (void)onGetViewerFinish:(NSNotification*)notif;

- (void)ResponseHandlerInit;
@end

@implementation ViewerViewController (Private)
- (void)onAddViewerFinish:(NSNotification*)notif
{
    NSDictionary* resp = [notif userInfo];
    NSString* res_code = [resp objectForKey:@"res_code"];
    NSString* status = [resp objectForKey:@"status"];
    if ([res_code isEqualToString:@"200"]) {
        if ([status isEqualToString:@"C_ERx0000"]) {
            [[LinphoneAppDelegate sharedInstance].vbellCloud getViewerForDevice:mac WithTag:0 returnToMainThread:YES];
        } else {
            UIAlertView *alert=[[UIAlertView alloc] initWithTitle:[resp objectForKey:@"title"] message:[resp objectForKey:@"detail"] delegate:nil cancelButtonTitle:nil otherButtonTitles:@"ok", nil];
            [alert show];
            [alert release];
        }
    } else {
        UIAlertView *alert=[[UIAlertView alloc] initWithTitle:@"伺服器忙碌中" message:@"請稍後再試" delegate:nil cancelButtonTitle:nil otherButtonTitles:@"確定", nil];
        [alert show];
        [alert release];
    }
    [[LinphoneAppDelegate sharedInstance] HideBusyView];
}

- (void)onRemoveViewerFinish:(NSNotification*)notif
{
    NSDictionary* resp = [notif userInfo];
    NSString* res_code = [resp objectForKey:@"res_code"];
    NSString* status = [resp objectForKey:@"status"];
    if ([res_code isEqualToString:@"200"]) {
        if ([status isEqualToString:@"C_ERx0000"]) {
            [[LinphoneAppDelegate sharedInstance].vbellCloud getViewerForDevice:mac WithTag:0 returnToMainThread:YES];
        } else {
            UIAlertView *alert=[[UIAlertView alloc] initWithTitle:[resp objectForKey:@"title"] message:[resp objectForKey:@"detail"] delegate:nil cancelButtonTitle:nil otherButtonTitles:@"ok", nil];
            
            [alert show];
            [alert release];
        }
    } else {
        UIAlertView *alert=[[UIAlertView alloc] initWithTitle:@"伺服器忙碌中" message:@"請稍後再試" delegate:nil cancelButtonTitle:nil otherButtonTitles:@"確定", nil];
        [alert show];
        [alert release];
    }
    [[LinphoneAppDelegate sharedInstance] HideBusyView];
}

- (void)onGetViewerFinish:(NSNotification*)notif
{
    NSDictionary* resp = [notif userInfo];
    NSString* res_code = [resp objectForKey:@"res_code"];
    NSString* status = [resp objectForKey:@"status"];
    if ([res_code isEqualToString:@"200"]) {
        if ([status isEqualToString:@"C_ERx0000"]) {
            [viewList removeAllObjects];
            NSArray* ary = [resp objectForKey:@"viewer_list"];
            for(NSString *str in ary)
                [viewList addObject:str];
            [self.tableview reloadData];
        } else {
            UIAlertView *alert=[[UIAlertView alloc] initWithTitle:[resp objectForKey:@"title"] message:[resp objectForKey:@"detail"] delegate:nil cancelButtonTitle:nil otherButtonTitles:@"ok", nil];
            [alert show];
            [alert release];
        }
    } else {
        UIAlertView* alert = [[UIAlertView alloc] initWithTitle:@"伺服器忙碌中" message:@"請稍後再試" delegate:nil cancelButtonTitle:nil otherButtonTitles:@"確定", nil];
        [alert show];
        [alert release];
    }
    [[LinphoneAppDelegate sharedInstance] HideBusyView];
}

- (void)ResponseHandlerInit
{
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(onAddViewerFinish:) name:kAddViewerResponse object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(onRemoveViewerFinish:) name:kRemoveViewerResponse object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(onGetViewerFinish:) name:kGetViewerResponse object:nil];
}
@end

@implementation ViewerViewController
@synthesize mac;
@synthesize readerView;

- (UIStatusBarStyle)preferredStatusBarStyle
{
    return UIStatusBarStyleLightContent;
}

- (BOOL)prefersStatusBarHidden
{
    return YES;
}

#pragma mark - View Init

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    [self ResponseHandlerInit];
    viewList =[[NSMutableArray alloc] init];
    [[LinphoneAppDelegate sharedInstance].vbellCloud getViewerForDevice:mac WithTag:0 returnToMainThread:YES];
    _lab_title.text=[NSString stringWithFormat:@"%@%@",NSLocalizedString(SHARER, nil),NSLocalizedString(SETTING, nil)];
    [_button_addsh setTitle:[NSString stringWithFormat:@"%@%@",NSLocalizedString(ADD, nil),NSLocalizedString(SHARER, nil)] forState:UIControlStateNormal];
    [_button_back setTitle:NSLocalizedString(BACK, nil) forState:UIControlStateNormal];
}

- (void)viewWillLayoutSubviews
{
    [self.button_back setFrame:CGRectMake(0, 0, self.view.frame.size.width/2, self.button_back.frame.size.height)];
    [self.button_addsh setFrame:CGRectMake(self.button_back.frame.size.width, 0, self.view.frame.size.width/2, self.button_back.frame.size.height)];
}

#pragma mark - UITableViewDelegate

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

- (NSInteger)tableView:(UITableView *)_tableView numberOfRowsInSection:(NSInteger)section
{
    return [viewList count];
}

- (CGFloat)tableView:(UITableView *)_tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (self.view.frame.size.width >= 375) {
        return 60;
    } else if (self.view.frame.size.width <= 320) {
        return 50;
    }
    return 55;
}

- (UITableViewCell *)tableView:(UITableView *)_tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    UITableViewCell *cell = [_tableView dequeueReusableCellWithIdentifier:@"CELL"];
    if (cell == nil)
    {
        cell=[[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"CELL"];
        CGFloat _width = 0;
        CGFloat _height = 0;
        CGFloat _center_y = 0;
        if (self.view.frame.size.width >= 375) {
            _width = 70;
            _height = 40;
            _center_y = 30;
            [cell.textLabel setFont:[UIFont systemFontOfSize:18]];
        } else if (self.view.frame.size.width <= 320) {
            _width = 65;
            _height = 35;
            _center_y = 25;
        } else {
            _width = 60;
            _height = 30;
            _center_y = 27;
            [cell.textLabel setFont:[UIFont systemFontOfSize:16]];
        }
        UIButton *bt=[[UIButton alloc]initWithFrame:CGRectMake(0, 0, _width, _height)];
        [bt setCenter:CGPointMake(self.view.frame.size.width - 15 - (_width/2), _center_y)];
        [bt setTitle:NSLocalizedString(DELETE, nil) forState:UIControlStateNormal];
        [bt addTarget:self action:@selector(button_delete:) forControlEvents:UIControlEventTouchUpInside];
        [bt setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
        [bt setShowsTouchWhenHighlighted:YES];
        [bt setBackgroundColor:[UIColor colorWithRed:1.0f green:0.5f blue:0.5f alpha:1.0f]];
        [bt.layer setCornerRadius:5];
        [bt setTag:indexPath.row];
        [cell addSubview:bt];
        [bt release];
    }
    cell.textLabel.text=[viewList objectAtIndex:indexPath.row];
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
}

- (IBAction)back:(id)sender
{
    if (QR_Flag)
    {
        [readerView stop];
        [readerView setHidden:YES];
        QR_Flag=NO;
    }
    else
        [self.navigationController popViewControllerAnimated:YES];
}

- (IBAction)AddView:(id)sender
{
    UIActionSheet *actionSheet=[[UIActionSheet alloc]initWithTitle:[NSString stringWithFormat:@"%@%@",NSLocalizedString(ADD, nil),NSLocalizedString(SHARER, nil)] delegate:self cancelButtonTitle:NSLocalizedString(CANCEL, nil) destructiveButtonTitle:nil otherButtonTitles:NSLocalizedString(SCAN, nil),NSLocalizedString(INPUT_ACC_SHARE, nil), nil];
    [actionSheet showInView:self.view];
    [actionSheet release];
}

 -(IBAction)button_delete:(id)sender
{
    UIButton *bt=sender;
    UIAlertView *alert=[[UIAlertView alloc]initWithTitle:NSLocalizedString(DELETE, nil) message:[NSString stringWithFormat:@"%@%@%@?",NSLocalizedString(SURE, nil),NSLocalizedString(DELETE, nil),NSLocalizedString(ACC, nil)] delegate:self cancelButtonTitle:NSLocalizedString(CANCEL, nil) otherButtonTitles:NSLocalizedString(OK, nil), nil];
    [alert setTag:20+bt.tag];
    [alert show];
    [alert release];
}

#pragma mark - UIActionSheetDelegate

- (void)actionSheet:(UIActionSheet *)actionSheet clickedButtonAtIndex:(NSInteger)buttonIndex
{
    if (buttonIndex==0)
    {
        QR_Flag=YES;
        [ZBarReaderView class];
        readerView.readerDelegate = self;
        if (cameraSim != nil)
            [cameraSim release];
        cameraSim = [[ZBarCameraSimulator alloc]
                     initWithViewController: self];
        cameraSim.readerView = readerView;
        [readerView setHidden:NO];
        [readerView start];
    }
    else if (buttonIndex==1)
    {
        UIAlertView *alert=[[UIAlertView alloc] initWithTitle:[NSString stringWithFormat:@"%@%@",NSLocalizedString(ADD, nil),NSLocalizedString(SHARER, nil)] message:NSLocalizedString(INPUT_ACC_SHARE, nil) delegate:self cancelButtonTitle:NSLocalizedString(CANCEL, nil) otherButtonTitles:NSLocalizedString(OK, nil), nil];
        [alert setAlertViewStyle:UIAlertViewStylePlainTextInput];
        [alert setTag:1];
        [alert show];
        [alert release];
    }
}

#pragma mark - UIAlertDelegate

- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    if (alertView.tag==1)
    {
        if (buttonIndex==1)
        {
            [[LinphoneAppDelegate sharedInstance].vbellCloud addViewerForDevice:mac withViewer:[alertView textFieldAtIndex:0].text WithTag:0 returnToMainThread:YES];
            [[LinphoneAppDelegate sharedInstance] ShowBusyViewWithMessage:[NSString stringWithFormat:@"%@%@",NSLocalizedString(ADDING, nil),NSLocalizedString(WAIT, nil)]];
        }
    }
    else if (alertView.tag>=20)
    {
        if (buttonIndex==1)
        {
            [[LinphoneAppDelegate sharedInstance] ShowBusyViewWithMessage:[NSString stringWithFormat:@"%@%@",NSLocalizedString(DELETING, nil),NSLocalizedString(WAIT, nil)]];
            [[LinphoneAppDelegate sharedInstance].vbellCloud removeViewerForDevice:mac withViewer:[viewList objectAtIndex:alertView.tag%20] WithTag:0 returnToMainThread:YES];
        }
    }
}

#pragma mark - ZBarReaderDelegate

- (void) readerView: (ZBarReaderView*)view didReadSymbols:(ZBarSymbolSet*)syms fromImage: (UIImage*)img
{
    for(ZBarSymbol *sym in syms)
    {
        [NSObject cancelPreviousPerformRequestsWithTarget:self selector:@selector(read_down) object:nil];
        _lab.text=sym.data;
        QR_Flag=NO;
        break;
    }
    [self performSelector:@selector(read_down) withObject:nil afterDelay:1.5];
}

-(void)read_down
{
    [readerView stop];
    [readerView setHidden:YES];
    UIAlertView *alert=[[UIAlertView alloc] initWithTitle:[NSString stringWithFormat:@"%@%@",NSLocalizedString(ADD, nil),NSLocalizedString(SHARER, nil)] message:NSLocalizedString(INPUT_ACC_SHARE, nil) delegate:self cancelButtonTitle:NSLocalizedString(CANCEL, nil) otherButtonTitles:NSLocalizedString(OK, nil), nil];
    [alert setAlertViewStyle:UIAlertViewStylePlainTextInput];
    [alert textFieldAtIndex:0].text=_lab.text;
    [alert setTag:1];
    [alert show];
    [alert release];
}

- (void)dealloc {
    [[NSNotificationCenter defaultCenter] removeObserver:self];
    [readerView release];
    [cameraSim release];
    [mac release];
    [_tableview release];
    [_lab release];
    [_button_back release];
    [_button_addsh release];
    [_lab_title release];
    [super dealloc];
}
@end
