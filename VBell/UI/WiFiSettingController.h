//
//  WiFiSettingController.h
//  linphone
//
//  Created by APPLE on 2014/11/6.
//
//

#ifndef _WiFiSettingController_h_
#define _WiFiSettingController_h_

#import <UIKit/UIKit.h>
#import <VBellCloud/VBellCloud.h>
@interface WiFiSettingController : UIViewController<UITextFieldDelegate,UIPickerViewDataSource,UIPickerViewDelegate,ScanDP>
{
    NSInteger nowPage,nowtext;
    NSString *mac,*model;
    NSMutableArray *picker_list;
    ScanDP *sdp;
    BOOL Find;
    NSString *scan_ip;
}
@property (retain, nonatomic) NSString *mac;
@property (retain, nonatomic) NSString *model;
@property (retain, nonatomic) IBOutlet UIScrollView *scroll;
@property (retain, nonatomic) IBOutlet UIButton *button_next;
@property (retain, nonatomic) IBOutlet UIButton *button_back;
@property (retain, nonatomic) IBOutlet UIView *view1;
@property (retain, nonatomic) IBOutlet UILabel *lab_info;
@property (retain, nonatomic) IBOutlet UIView *view2;
@property (retain, nonatomic) IBOutlet UILabel *lab1;
@property (retain, nonatomic) IBOutlet UILabel *lab2;
@property (retain, nonatomic) IBOutlet UILabel *lab3;
@property (retain, nonatomic) IBOutlet UILabel *lab4;
@property (retain, nonatomic) IBOutlet UITextField *text1;
@property (retain, nonatomic) IBOutlet UITextField *text2;
@property (retain, nonatomic) IBOutlet UITextField *text3;
@property (retain, nonatomic) IBOutlet UITextField *text4;
@property (retain, nonatomic) IBOutlet UIView *view3;
@property (retain, nonatomic) IBOutlet UILabel *lab_info2;
@property (retain, nonatomic) IBOutlet UIPickerView *picker;
@property (retain, nonatomic) IBOutlet UIView *ass_view;
@property (retain, nonatomic) IBOutlet UIButton *button_done;
@property (retain, nonatomic) IBOutlet NSLayoutConstraint *layy;
@property (retain, nonatomic) IBOutlet NSLayoutConstraint *layout;
- (IBAction)ad_set_down:(id)sender;
@property (retain, nonatomic) IBOutlet UISwitch *switch_show;
@property (retain, nonatomic) IBOutlet UILabel *lab_show;
- (IBAction)sw_down:(id)sender;
@end

#endif
