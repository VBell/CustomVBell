//
//  HistoryViewController.m
//  linphone
//
//  Created by APPLE on 2016/6/22.
//
//

#import "HistoryViewController.h"
#import "HistoryViewCell.h"
#import "HistoryViewHeader.h"
#import "HistoryCaptureViewController.h"
#import "LinphoneAppDelegate.h"

@interface HistoryViewController (Private)
- (void)getEventLogForAccount:(NSString*)account withPassword:(NSString*)password;
- (void)onGetEventLogFinish:(NSNotification*)notif;

- (void)ResponseHandlerInit;
@end

@implementation HistoryViewController (Private)
- (void)getEventLogForAccount:(NSString*)account withPassword:(NSString*)password
{
    if (!account || !password)
        return;
    [[LinphoneAppDelegate sharedInstance].vbellCloud getEventLogWithTag:0 returnToMainThread:YES];
    [self.loadingIndicator startAnimating];
}

- (void)onGetEventLogFinish:(NSNotification*)notif
{
    [self.loadingIndicator stopAnimating];
    NSDictionary* resp = [notif userInfo];
    NSString* res_code = [resp objectForKey:@"res_code"];
    NSString* status = [resp objectForKey:@"status"];
    if ([res_code isEqualToString:@"200"]) {
        if (status && [status isEqualToString:@"GCE0000"]) {
            NSMutableArray<NSMutableArray<NSMutableDictionary*>*>* _tableList = [resp objectForKey:@"list"];
            if (_tableList) {
                if (tableList) {
                    [tableList release];
                }
                tableList = [[NSMutableArray<NSMutableArray<NSMutableDictionary*>*> alloc] initWithArray:_tableList copyItems:YES];
                [self.historyTableView reloadData];
            }
        } else {
            NSLog(@"onGetEventLogFinish: Unknown format.");
        }
    } else {
        NSLog(@"onGetEventLogError: res_code=%@", res_code);
    }
}

- (void)ResponseHandlerInit
{
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(onGetEventLogFinish:) name:kGetEventLogResponse object:nil];
}
@end

@implementation HistoryViewController

- (UIStatusBarStyle)preferredStatusBarStyle
{
    return UIStatusBarStyleLightContent;
}

- (BOOL)prefersStatusBarHidden
{
    return YES;
}

#pragma mark - View Init

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    [self ResponseHandlerInit];
    [self.historyTableView registerClass:[UITableViewHeaderFooterView class] forHeaderFooterViewReuseIdentifier:@"TableViewSectionHeaderViewIdentifier"];
    [self.top_label setText:NSLocalizedString(HomeViewController_HistoryLabel,nil)];
    [self.button_back setTitle:NSLocalizedString(BACK, nil) forState:UIControlStateNormal];
    [self getEventLogForAccount:[UserInfo getStringForKey:DG_ACC] withPassword:[UserInfo getStringForKey:DG_PWD]];
}

- (IBAction)onBackClick:(id)sender
{
    [self.navigationController popViewControllerAnimated:YES];
}

- (void)dealloc {
    [[NSNotificationCenter defaultCenter] removeObserver:self];
    [self.top_label release];
    [self.button_back release];
    [self.emptyInfo release];
    [self.historyTableView release];
    [self.loadingIndicator release];
    [self.contentArea_Empty release];
    [self.contentArea_List release];
    [tableList release];
    [super dealloc];
}

#pragma mark - UITableViewDelegate

- (NSInteger)numberOfSectionsInTableView:(UITableView *)_tableView
{
    return [tableList count];
}

- (NSInteger)tableView:(UITableView *)_tableView numberOfRowsInSection:(NSInteger)section
{
    return [[tableList objectAtIndex:section] count];
}

- (CGFloat)tableView:(UITableView *)_tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (self.view.frame.size.width >= 375) {
        return 85;
    } else if (self.view.frame.size.width <= 320) {
        return 75;
    }
    return 80;
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    if (self.view.frame.size.width >= 375) {
        return 40;
    } else if (self.view.frame.size.width <= 320) {
        return 30;
    }
    return 35;
}

- (nullable UIView *)tableView:(UITableView *)_tableView viewForHeaderInSection:(NSInteger)section
{
    static NSString *headerReuseIdentifier = @"TableViewSectionHeaderViewIdentifier";
    
    // Reuse the instance that was created in viewDidLoad, or make a new one if not enough.
    UITableViewHeaderFooterView *sectionHeaderView = [_tableView dequeueReusableHeaderFooterViewWithIdentifier:headerReuseIdentifier];
    UILabel *titleLabel = (UILabel *)[sectionHeaderView.contentView viewWithTag:1];
    if (titleLabel == nil) {
        UIColor *backgroundColor = [UIColor colorWithRed:155.0/255.0f green:155.0/255.0f blue:155.0/255.0f alpha:1.0];
        sectionHeaderView.contentView.backgroundColor = backgroundColor;
        CGFloat _height = 0;
        if (self.view.frame.size.width >= 375) {
            _height = 40;
        } else if (self.view.frame.size.width <= 320) {
            _height = 30;
        } else {
            _height = 35;
        }
        titleLabel = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, self.view.frame.size.width, _height)];
        titleLabel.textColor = [UIColor whiteColor];
        titleLabel.backgroundColor = backgroundColor;
        titleLabel.tag = 1;
        titleLabel.font = [UIFont systemFontOfSize:25.0f];
        [titleLabel setTextAlignment:NSTextAlignmentCenter];
        [sectionHeaderView.contentView addSubview:titleLabel];
    }
    [titleLabel setText:[[[tableList objectAtIndex:section] objectAtIndex:0] objectForKey:@"Date"]];
    return sectionHeaderView;
}

- (UITableViewCell *)tableView:(UITableView *)_tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    HistoryViewCell *cell = [_tableView dequeueReusableCellWithIdentifier:@"HistoryViewCell"];
    if (cell == nil) {
        cell = [[[NSBundle mainBundle]loadNibNamed:@"HistoryViewCell" owner:self options:nil] lastObject];
    }
    if (self.view.frame.size.width >= 375) {
        [cell initWithDictionary:[[tableList objectAtIndex:[indexPath section]] objectAtIndex:[indexPath row]] size:DeviceWindowSize_6];
    } else if (self.view.frame.size.width <= 320) {
        [cell initWithDictionary:[[tableList objectAtIndex:[indexPath section]] objectAtIndex:[indexPath row]] size:DeviceWindowSize_4];
    } else {
        [cell initWithDictionary:[[tableList objectAtIndex:[indexPath section]] objectAtIndex:[indexPath row]] size:DeviceWindowSize_5];
    }
    return cell;
}

- (void)tableView:(UITableView *)_tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    NSString* snapshot_key = [[[tableList objectAtIndex:[indexPath section]] objectAtIndex:[indexPath row]] objectForKey:@"key"];
    if (snapshot_key && [snapshot_key length] > 0) {
        HistoryCaptureViewController* hcvc = [[HistoryCaptureViewController alloc] init];
        [self.navigationController pushViewController:hcvc animated:YES];
        [hcvc setDeviceDetail:[[tableList objectAtIndex:[indexPath section]] objectAtIndex:[indexPath row]]];
        [hcvc release];
    }
    [_tableView deselectRowAtIndexPath:indexPath animated:YES];
}
@end
