//
//  HistoryCaptureViewController.h
//  linphone
//
//  Created by APPLE on 2014/8/19.
//
//

#ifndef _HistoryCaptureViewController_h_
#define _HistoryCaptureViewController_h_

#import <UIKit/UIKit.h>

@interface HistoryCaptureViewController : UIViewController
{
    NSMutableDictionary* deviceDetail;
}
@property (retain, nonatomic) IBOutlet UILabel* top_label;
@property (retain, nonatomic) IBOutlet UIButton* button_back;
@property (retain, nonatomic) IBOutlet UIView* timeBar;
@property (retain, nonatomic) IBOutlet UIImageView* snapshot;
@property (retain, nonatomic) IBOutlet UILabel* event_time;

- (void)setDeviceDetail:(NSMutableDictionary*)_deatil;
@end

#endif
