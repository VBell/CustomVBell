//
//  CallViewController.h
//  linphone
//
//  Created by APPLE on 2014/7/14.
//
//

#ifndef _CallViewController_h_
#define _CallViewController_h_

#import <UIKit/UIKit.h>
#import <VBellCloud/VBellCloud.h>

typedef enum _CallViewMode
{
    CallViewMode_None,
    CallViewMode_Incoming,
    CallViewMode_Outgoing,
    CallViewMode_Answered,
    CallViewMode_Hangup,
    CallViewMode_Connected
} CallViewMode;

@interface CallViewController : UIViewController
{
    VideoZoomHandler* videoZoomHandler;
    BOOL isWallPad;
    CallViewMode current_mode;
    NSString* name;
    NSString* mac;
    NSString* model;
    BOOL prepareDismiss;
}
@property (retain, nonatomic) IBOutlet UIImageView *image_flag;
@property (nonatomic, retain) IBOutlet UIView* videoGroup;
@property (nonatomic, retain) IBOutlet UIView* videoView;
@property (nonatomic, retain) IBOutlet UIButton *accepted;
@property (nonatomic, retain) IBOutlet UIButton *decliented;
@property (retain, nonatomic) IBOutlet UIButton *open_door;
@property (retain, nonatomic) IBOutlet UILabel *caller_id;
@property (retain, nonatomic) IBOutlet ImageViewDownLoader *image_preview;

- (void)InitWithName:(NSString*)_name forMAC:(NSString*)_mac forModel:(NSString*)_model;
- (void)ChangeModeTo:(CallViewMode)new_mode;
@end

#endif
