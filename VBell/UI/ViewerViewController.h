//
//  ViewerViewController.h
//  linphone
//
//  Created by APPLE on 2014/8/26.
//
//

#ifndef _ViewerViewController_h_
#define _ViewerViewController_h_

#import <UIKit/UIKit.h>
#import <VBellCloud/VBellCloud.h>
#import "ZBarSDK.h"

@interface ViewerViewController : UIViewController<UITableViewDataSource,UITableViewDelegate,UIActionSheetDelegate,UIAlertViewDelegate,ZBarReaderDelegate,ZBarReaderViewDelegate>
{
    NSMutableArray *viewList;
    NSString *mac;
    ZBarReaderView *readerView;
    ZBarCameraSimulator *cameraSim;
    BOOL QR_Flag;
}
@property (retain, nonatomic) NSString *mac;
@property (retain, nonatomic) IBOutlet UITableView *tableview;
@property (retain, nonatomic) IBOutlet ZBarReaderView *readerView;
@property (retain, nonatomic) IBOutlet UILabel *lab;
@property (retain, nonatomic) IBOutlet UIButton *button_back;
@property (retain, nonatomic) IBOutlet UIButton *button_addsh;
@property (retain, nonatomic) IBOutlet UILabel *lab_title;
@end

#endif
