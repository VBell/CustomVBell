//
//  HomeViewController.m
//  linphone
//
//  Created by APPLE on 2014/7/14.
//
//

#import "HomeViewController.h"
#import "ViewerViewController.h"
#import "LinphoneAppDelegate.h"
#import "AddCamViewController.h"
#import "MyAlertView.h"
#import "WiFiSettingController.h"
#import "HistoryViewController.h"
#import "FindIPViewController.h"
#import "RecordingViewController.h"

@interface HomeViewController (Private)
- (void)onGetDeviceListFinish:(NSNotification*)notif;
- (void)onDeleteDeviceFinish:(NSNotification*)notif;
- (void)onGetDeviceStatusFinish:(NSNotification*)notif;
- (void)onSetDeviceStatusFinish:(NSNotification*)notif;

- (void)ResponseHandlerInit;
@end

@implementation HomeViewController (Private)
- (void)onGetDeviceListFinish:(NSNotification*)notif
{
    NSDictionary* resp = [notif userInfo];
    NSString* res_code = [resp objectForKey:@"res_code"];
    NSString* status = [resp objectForKey:@"status"];
    if ([res_code isEqualToString:@"200"] && [status isEqualToString:@"C_ERx0000"]) {
        [[LinphoneAppDelegate sharedInstance] UpdateDeviceList:[resp objectForKey:@"cam_list"]];
        [self RefreshScroll];
    } else {
        UIAlertView *alert=[[UIAlertView alloc] initWithTitle:NSLocalizedString(CON_ERROR_TITLE, nil) message:NSLocalizedString(CON_ERROR_MESSAGE, nil) delegate:nil cancelButtonTitle:nil otherButtonTitles:NSLocalizedString(OK, nil), nil];
        [alert show];
        [alert release];
    }
    [[LinphoneAppDelegate sharedInstance] HideBusyView];
}

- (void)onDeleteDeviceFinish:(NSNotification*)notif
{
    NSDictionary* resp = [notif userInfo];
    NSString* res_code = [resp objectForKey:@"res_code"];
    NSString* status = [resp objectForKey:@"status"];
    if ([res_code isEqualToString:@"200"]) {
        if ([status isEqualToString:@"C_ERx0000"]) {
            [[LinphoneAppDelegate sharedInstance].vbellCloud getDeviceListWithTag:0 checkError:YES returnToMainThread:YES];
        }
    } else {
        [[LinphoneAppDelegate sharedInstance] HideBusyView];
        UIAlertView *alert=[[UIAlertView alloc] initWithTitle:NSLocalizedString(CON_ERROR_TITLE, nil) message:NSLocalizedString(CON_ERROR_MESSAGE, nil) delegate:nil cancelButtonTitle:nil otherButtonTitles:NSLocalizedString(OK, nil), nil];
        [alert show];
        [alert release];
    }
}

- (void)onGetDeviceStatusFinish:(NSNotification*)notif
{
    NSDictionary* resp = [notif userInfo];
    NSString* res_code = [resp objectForKey:@"res_code"];
    if ([res_code isEqualToString:@"200"]) {
        if (camState)
            [camState release];
        camState = [[NSMutableDictionary alloc] initWithDictionary:[resp objectForKey:@"cam_status"]];
        UIAlertView *alert=[[UIAlertView alloc] initWithTitle:NSLocalizedString(CHANGE_DP_NAME,nil) message:NSLocalizedString(INPUT_NEW_NAME,nil) delegate:self cancelButtonTitle:NSLocalizedString(CANCEL,nil) otherButtonTitles:NSLocalizedString(OK,nil), nil];
        [alert setAlertViewStyle:UIAlertViewStylePlainTextInput];
        [alert textFieldAtIndex:0].text = [camState objectForKey:@"cameraName"];
        [alert setTag:1];
        [alert show];
        [alert release];
        [[LinphoneAppDelegate sharedInstance] HideBusyView];
    } else {
        [[LinphoneAppDelegate sharedInstance] HideBusyView];
        UIAlertView *alert=[[UIAlertView alloc] initWithTitle:NSLocalizedString(CON_ERROR_TITLE, nil) message:NSLocalizedString(CON_ERROR_MESSAGE, nil) delegate:nil cancelButtonTitle:nil otherButtonTitles:NSLocalizedString(OK, nil), nil];
        [alert show];
        [alert release];
    }
}

- (void)onSetDeviceStatusFinish:(NSNotification*)notif
{
    NSDictionary* resp = [notif userInfo];
    NSString* res_code = [resp objectForKey:@"res_code"];
    if ([res_code isEqualToString:@"200"]) {
        [[LinphoneAppDelegate sharedInstance].vbellCloud getDeviceListWithTag:0 checkError:NO returnToMainThread:YES];
    } else {
        [[LinphoneAppDelegate sharedInstance] HideBusyView];
        UIAlertView *alert=[[UIAlertView alloc] initWithTitle:NSLocalizedString(CON_ERROR_TITLE, nil) message:NSLocalizedString(CON_ERROR_MESSAGE, nil) delegate:nil cancelButtonTitle:nil otherButtonTitles:NSLocalizedString(OK, nil), nil];
        [alert show];
        [alert release];
    }
}

- (void)ResponseHandlerInit
{
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(onGetDeviceListFinish:) name:kGetDeviceListResponse object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(onDeleteDeviceFinish:) name:kDeleteDeviceResponse object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(onGetDeviceStatusFinish:) name:kGetDeviceStatusResponse object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(onSetDeviceStatusFinish:) name:kSetDeviceStatusResponse object:nil];
}
@end

@implementation HomeViewController

- (UIStatusBarStyle)preferredStatusBarStyle
{
    return UIStatusBarStyleLightContent;
}

- (BOOL)prefersStatusBarHidden
{
    return YES;
}

#pragma mark - View Init

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

-(BOOL)canRecord
{
    __block BOOL bCanRecord = YES;
    if ([[[UIDevice currentDevice] systemVersion] compare:@"7.0"] != NSOrderedAscending)
    {
        AVAudioSession *audioSession = [AVAudioSession sharedInstance];
        if ([audioSession respondsToSelector:@selector(requestRecordPermission:)])
        {
            [audioSession performSelector:@selector(requestRecordPermission:) withObject:^(BOOL granted) {
                if (granted) {
                     bCanRecord = YES;
                 } else {
                     bCanRecord = NO;
                     dispatch_async(dispatch_get_main_queue(), ^{
                         [[[[UIAlertView alloc] initWithTitle:nil message:@"app需要使用到麥克風。\n請開啟麥克風-設定/隱私/麥克風" delegate:nil cancelButtonTitle:@"關閉" otherButtonTitles:nil] autorelease] show];
                     });
                 }
             }];
        }
    }
    return bCanRecord;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    [self ResponseHandlerInit];
    [self canRecord];
    [_button_adddp setTitle:NSLocalizedString(ADDDP, nil) forState:UIControlStateNormal];
    [_button_setting setTitle:NSLocalizedString(SETTING, nil) forState:UIControlStateNormal];
    [self.button_history setTitle:NSLocalizedString(HomeViewController_HistoryLabel,nil) forState:UIControlStateNormal];
    _title1.text=[NSString stringWithFormat:@"%@ DoorPhone",NSLocalizedString(MY, nil)];
    _title2.text=[NSString stringWithFormat:@"%@ DoorPhone",NSLocalizedString(SHARE_TO_ME, nil)];
    if (![UserInfo getBooleanForKey:HELP])
    {
        [UserInfo setBoolean:YES forKey:HELP];
        [self performSelector:@selector(ShowHelp) withObject:nil afterDelay:1];
    }
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    qr_show = NO;
    [[LinphoneAppDelegate sharedInstance].vbellCloud getDeviceListWithTag:0 checkError:YES returnToMainThread:YES];
    NSString* ssid = @"";
    [UserInfo setString:ssid forKey:@"SSID"];
}

- (void)viewWillLayoutSubviews
{
    [self.button_history setFrame:CGRectMake(self.tab_view.frame.size.width/2 - self.tab_view.frame.size.width/6, 0.0f, self.tab_view.frame.size.width/3, self.button_history.frame.size.height)];
    [self.button_setting setFrame:CGRectMake(0.0f, 0.0f, self.button_history.frame.size.width, self.button_history.frame.size.height)];
    [self.button_adddp setFrame:CGRectMake(self.tab_view.frame.size.width - self.button_history.frame.size.width, 0.0f, self.button_history.frame.size.width, self.button_history.frame.size.height)];
}

- (void)RefreshScroll
{
    for (ContentView *vv in _scroll1.subviews)
    {
        if ([vv isKindOfClass:[ContentView class]])
            [vv removeFromSuperview];
    }
    
    for (ContentView *vv in _scroll2.subviews)
    {
        if ([vv isKindOfClass:[ContentView class]])
            [vv removeFromSuperview];
    }
    NSMutableArray<NSMutableDictionary*>* _ownList = [[LinphoneAppDelegate sharedInstance] GetOwnDeviceList];
    NSMutableArray<NSMutableDictionary*>* _viewList = [[LinphoneAppDelegate sharedInstance] GetViewDeviceList];
    if (!_ownList || !_viewList) {
        NSLog(@"Error: device list format error.");
        return;
    }
    [_scroll1 setContentSize:CGSizeMake(_scroll1.frame.size.width*[_ownList count], _scroll1.frame.size.height)];
    [_scroll2 setContentSize:CGSizeMake(_scroll2.frame.size.width*[_viewList count], _scroll2.frame.size.height)];
    [_page1 setNumberOfPages:[_ownList count]];
    [_page2 setNumberOfPages:[_viewList count]];
    for(int i = 0; i < [_ownList count]; i++)
    {
        ContentView* vv = [[ContentView alloc] initContentView];
        vv.frame = CGRectMake(_scroll1.frame.size.width*i, 0, _scroll1.frame.size.width, _scroll1.frame.size.height);
        [vv setTag:i];
        [vv.setting setTag:i];
        NSDictionary* dg_dic = [_ownList objectAtIndex:i];
        [vv.name setText:[dg_dic objectForKey:@"NAME"]];
        [vv.mac setText:[dg_dic objectForKey:@"MAC"]];
        if ([[dg_dic objectForKey:@"NAME"] isEqualToString:@""] || [[dg_dic objectForKey:@"NAME"] isEqualToString:[dg_dic objectForKey:@"MAC"]])
            [vv.name setText:[NSString stringWithFormat:@"Door Phone %d",i+1]];
        [vv.tap addTarget:self action:@selector(dp_down_owner:)];
        [vv.setting addTarget:self action:@selector(CallActionSheet:) forControlEvents:UIControlEventTouchUpInside];
        NSString* model = [[_ownList objectAtIndex:i] objectForKey:@"MODEL"];
        if ([[model uppercaseString] isEqualToString:@"DP106"])
            [vv.image setImage:[UIImage imageNamed:@"dp106.png"]];
        else if ([[model uppercaseString] isEqualToString:@"DP107"])
            [vv.image setImage:[UIImage imageNamed:@"dp107.png"]];
        else if ([[model uppercaseString] isEqualToString:@"WP101"])
            [vv.image setImage:[UIImage imageNamed:@"wp101.png"]];
        else
            [vv.image setImage:[UIImage imageNamed:@"dp104.png"]];
        [_scroll1 addSubview:vv];
    }
    
    for(int i = 0; i < [_viewList count]; i++)
    {
        ContentView* vv = [[ContentView alloc] initContentView];
        vv.frame = CGRectMake(_scroll2.frame.size.width*i, 0, _scroll2.frame.size.width, _scroll2.frame.size.height);
        [vv setTag:i];
        [vv.setting setHidden:YES];
        NSDictionary* dg_dic = [_viewList objectAtIndex:i];
        [vv.name setText:[dg_dic objectForKey:@"NAME"]];
        [vv.mac setText:[dg_dic objectForKey:@"MAC"]];
        if ([[dg_dic objectForKey:@"NAME"] isEqualToString:@""] || [[dg_dic objectForKey:@"NAME"] isEqualToString:[dg_dic objectForKey:@"MAC"]])
            [vv.name setText:[NSString stringWithFormat:@"Door Phone %d",i+1]];
        [vv.tap addTarget:self action:@selector(dp_down_viewer:)];
        NSString* model = [[_viewList objectAtIndex:i] objectForKey:@"MODEL"];
        if ([[model uppercaseString] isEqualToString:@"DP106"])
            [vv.image setImage:[UIImage imageNamed:@"dp106.png"]];
        else if ([[model uppercaseString] isEqualToString:@"DP107"])
            [vv.image setImage:[UIImage imageNamed:@"dp107.png"]];
        else if ([[model uppercaseString] isEqualToString:@"WP101"])
            [vv.image setImage:[UIImage imageNamed:@"wp101.png"]];
        else
            [vv.image setImage:[UIImage imageNamed:@"dp104.png"]];
        [_scroll2 addSubview:vv];
    }
    
    if ([_ownList count] == 0)
    {
        ContentView* vv = [[ContentView alloc] initContentView];
        vv.frame = CGRectMake(0, 0, _scroll1.frame.size.width, _scroll1.frame.size.height);
        [vv setTag:0];
        [vv.title setText:NSLocalizedString(NODP_ADDDP, nil)];
        [vv SetNotDP];
        [vv.tap addTarget:self action:@selector(button_add_down:)];
        [_scroll1 addSubview:vv];
    }
}

- (IBAction)dp_down_owner:(id)sender
{
    UITapGestureRecognizer* tap = sender;
    [[LinphoneAppDelegate sharedInstance] ShowBusyViewWithMessage:[NSString stringWithFormat:@"%@,%@",NSLocalizedString(CONNECTING, nil),NSLocalizedString(WAIT, nil)]];
    NSMutableArray<NSMutableDictionary*>* _ownList = [[LinphoneAppDelegate sharedInstance] GetOwnDeviceList];
    if (tap.view.tag < [_ownList count])
    {
        NSString* incall_mac = [[_ownList objectAtIndex:tap.view.tag] objectForKey:@"MAC"];
        [[LinphoneAppDelegate sharedInstance].vbellCloud Call:incall_mac];
    }
    else
        [self button_add_down:nil];
}

- (IBAction)dp_down_viewer:(id)sender
{
    UITapGestureRecognizer* tap = sender;
    [[LinphoneAppDelegate sharedInstance] ShowBusyViewWithMessage:[NSString stringWithFormat:@"%@,%@",NSLocalizedString(CONNECTING, nil),NSLocalizedString(WAIT, nil)]];
    NSMutableArray<NSMutableDictionary*>* _viewList = [[LinphoneAppDelegate sharedInstance] GetViewDeviceList];
    if (tap.view.tag < [_viewList count])
    {
        NSString* incall_mac = [[_viewList objectAtIndex:tap.view.tag] objectForKey:@"MAC"];
        [[LinphoneAppDelegate sharedInstance].vbellCloud Call:incall_mac];
    }
}

#pragma mark - UIScrollViewDelegate

- (void)scrollViewDidScroll:(UIScrollView *)sender
{
    [UIView animateWithDuration:0.1 animations:^(void)
     {
         if (_scroll1==sender)
         {
             CGFloat width = _scroll1.frame.size.width;
             NSInteger currentPage = (_scroll1.contentOffset.x / width);
             [_page1 setCurrentPage:currentPage];
         }
         else
         {
             CGFloat width = _scroll2.frame.size.width;
             NSInteger currentPage = (_scroll2.contentOffset.x / width);
             [_page2 setCurrentPage:currentPage];
         }
     }];
}

- (IBAction)changeCurrentPage:(UIPageControl *)sender
{
    if (_page1==sender)
    {
        NSInteger page = _page1.currentPage;
        CGFloat width, height;
        width = _scroll1.frame.size.width;
        height = _scroll1.frame.size.height;
        CGRect frame = CGRectMake(width*page, 0, width, height);
        [_scroll1 scrollRectToVisible:frame animated:YES];
    }
    else
    {
        NSInteger page = _page2.currentPage;
        CGFloat width, height;
        width = _scroll2.frame.size.width;
        height = _scroll2.frame.size.height;
        CGRect frame = CGRectMake(width*page, 0, width, height);
        [_scroll2 scrollRectToVisible:frame animated:YES];
    }
}

#pragma mark - CallStatusNotification

- (IBAction)button_add_down:(id)sender
{
    AddCamViewController *acvc=[[AddCamViewController alloc] init];
    [self.navigationController pushViewController:acvc animated:YES];
    [acvc release];
}

- (IBAction)button_setting_down:(id)sender
{
    SettingViewController *svc=[[SettingViewController alloc] init];
    [self.navigationController pushViewController:svc animated:YES];
    [svc release];
}

- (IBAction)button_history_down:(id)sender
{
    HistoryViewController* hvc = [[HistoryViewController alloc] init];
    [self.navigationController pushViewController:hvc animated:YES];
    [hvc release];
}

- (void)ShowHelp
{
    MyAlertView *mv=[[MyAlertView alloc]initWithParent:self.view];
    [mv show];
}

- (void)CallActionSheet:(UIButton *)bt
{
    NSMutableArray<NSMutableDictionary*>* _ownList = [[LinphoneAppDelegate sharedInstance] GetOwnDeviceList];
    NSString *model=[[_ownList objectAtIndex:bt.tag] objectForKey:@"MODEL"];
    if ([[model uppercaseString] isEqualToString:@"DP107"]) {
        UIActionSheet *actionSheet = [[UIActionSheet alloc] initWithTitle:[NSString stringWithFormat:@"Door Phone %@",NSLocalizedString(SETTING, nil)] delegate:self cancelButtonTitle:NSLocalizedString(CANCEL, nil) destructiveButtonTitle:nil otherButtonTitles:NSLocalizedString(CHANGE_DP_NAME,nil),[NSString stringWithFormat:@"%@ %@",NSLocalizedString(SHARE, nil),NSLocalizedString(SETTING, nil)],[NSString stringWithFormat:@"%@ Door Phone",NSLocalizedString(DELETE, nil)], NSLocalizedString(FIND_IP, nil), [NSString stringWithFormat:@"WiFi %@",NSLocalizedString(SETTING, nil)], NSLocalizedString(RECORDING, nil), nil];
        [actionSheet setTag:bt.tag];
        [actionSheet showInView:self.view];
        [actionSheet release];
    }
    else if ([[model uppercaseString] isEqualToString:@"DP104W"] || [[model uppercaseString] isEqualToString:@"DP105"] || [[model uppercaseString] isEqualToString:@"DP106"] || [[model uppercaseString] isEqualToString:@"DP107"])
    {
        UIActionSheet *actionSheet = [[UIActionSheet alloc] initWithTitle:[NSString stringWithFormat:@"Door Phone %@",NSLocalizedString(SETTING, nil)] delegate:self cancelButtonTitle:NSLocalizedString(CANCEL, nil) destructiveButtonTitle:nil otherButtonTitles:NSLocalizedString(CHANGE_DP_NAME,nil),[NSString stringWithFormat:@"%@ %@",NSLocalizedString(SHARE, nil),NSLocalizedString(SETTING, nil)],[NSString stringWithFormat:@"%@ Door Phone",NSLocalizedString(DELETE, nil)], NSLocalizedString(FIND_IP, nil), [NSString stringWithFormat:@"WiFi %@",NSLocalizedString(SETTING, nil)], nil];
        [actionSheet setTag:bt.tag];
        [actionSheet showInView:self.view];
        [actionSheet release];
    }
    else
    {
        UIActionSheet *actionSheet=[[UIActionSheet alloc] initWithTitle:[NSString stringWithFormat:@"Door Phone %@",NSLocalizedString(SETTING, nil)] delegate:self cancelButtonTitle:NSLocalizedString(CANCEL, nil) destructiveButtonTitle:nil otherButtonTitles:NSLocalizedString(CHANGE_DP_NAME,nil),[NSString stringWithFormat:@"%@ %@",NSLocalizedString(SHARE, nil),NSLocalizedString(SETTING, nil)],[NSString stringWithFormat:@"%@ Door Phone",NSLocalizedString(DELETE, nil)], NSLocalizedString(FIND_IP, nil), nil];
        [actionSheet setTag:bt.tag];
        [actionSheet showInView:self.view];
        [actionSheet release];
    }
}

#pragma mark - UIActionSheetDelegate

- (void)actionSheet:(UIActionSheet *)actionSheet clickedButtonAtIndex:(NSInteger)buttonIndex
{
    NSMutableArray<NSMutableDictionary*>* _ownList = [[LinphoneAppDelegate sharedInstance] GetOwnDeviceList];
    if (buttonIndex==0)
    {
        [[LinphoneAppDelegate sharedInstance] ShowBusyViewWithMessage:NSLocalizedString(WAIT, nil)];
        [[LinphoneAppDelegate sharedInstance].vbellCloud getDeviceStatus:[[_ownList objectAtIndex:actionSheet.tag] objectForKey:@"MAC"] withTag:0 returnToMainThread:YES];
    }
    else if (buttonIndex==1)
    {
        ViewerViewController* viewer = [[ViewerViewController alloc] init];
        viewer.mac = [[_ownList objectAtIndex:actionSheet.tag] objectForKey:@"MAC"];
        [self.navigationController pushViewController:viewer animated:YES];
        [viewer release];
    }
    else if (buttonIndex==2)
    {
        UIAlertView* alert = [[UIAlertView alloc] initWithTitle:NSLocalizedString(DELETE, nil) message:[NSString stringWithFormat:@"%@ %@ Door Phone?",NSLocalizedString(SURE, nil),NSLocalizedString(DELETE, nil)] delegate:self cancelButtonTitle:NSLocalizedString(CANCEL, nil) otherButtonTitles:NSLocalizedString(OK, nil), nil];
        [alert setTag:20+actionSheet.tag];
        [alert show];
        [alert release];
    }
    else if (buttonIndex==3)
    {
        FindIPViewController* fipvc = [[FindIPViewController alloc] init];
        [fipvc setFindedDevice:[[_ownList objectAtIndex:actionSheet.tag] objectForKey:@"MAC"]];
        [self.navigationController pushViewController:fipvc animated:YES];
        [fipvc release];
    }
    else if (buttonIndex==4)
    {
        NSString *model=[[_ownList objectAtIndex:actionSheet.tag] objectForKey:@"MODEL"];
        if ([[model uppercaseString] isEqualToString:@"DP104W"] || [[model uppercaseString] isEqualToString:@"DP105"] || [[model uppercaseString] isEqualToString:@"DP106"] || [[model uppercaseString] isEqualToString:@"DP107"])
        {
            WiFiSettingController *wsc=[[WiFiSettingController alloc] init];
            wsc.mac=[[_ownList objectAtIndex:actionSheet.tag] objectForKey:@"MAC"];
            wsc.model=model;
            [self.navigationController pushViewController:wsc animated:YES];
            [wsc release];
        }
    }
    else if (buttonIndex==5)
    {
        //recording
        RecordingViewController* rvc = [[RecordingViewController alloc] init];
        [rvc setFindedDevice:[[_ownList objectAtIndex:actionSheet.tag] objectForKey:@"MAC"]];
        [self.navigationController pushViewController:rvc animated:YES];
        [rvc release];
    }
}

#pragma mark - UIAlertDelegate

- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    if (alertView.tag==1)
    {
        if (buttonIndex==1)
        {
            [[LinphoneAppDelegate sharedInstance] ShowBusyViewWithMessage:NSLocalizedString(WAIT, nil)];
            [[LinphoneAppDelegate sharedInstance].vbellCloud setDeviceStatus:[camState objectForKey:@"mac"] withName:[alertView textFieldAtIndex:0].text withState:camState withTag:0 returnToMainThread:YES];
        }
    }
    else if (alertView.tag>=20)
    {
        if (buttonIndex==1)
        {
            NSMutableArray<NSMutableDictionary*>* _ownList = [[LinphoneAppDelegate sharedInstance] GetOwnDeviceList];
            NSDictionary *cam_dic=[_ownList objectAtIndex:alertView.tag%20];
            [[LinphoneAppDelegate sharedInstance] ShowBusyViewWithMessage:[NSString stringWithFormat:@"%@,%@",NSLocalizedString(DELETING, nil),NSLocalizedString(WAIT, nil)]];
            [[LinphoneAppDelegate sharedInstance].vbellCloud deleteDevice:[cam_dic objectForKey:@"MAC"] withTag:0 returnToMainThread:YES];
        }
    }
}

- (void)dealloc
{
    [[NSNotificationCenter defaultCenter] removeObserver:self];
    [self.button_adddp release];
    [self.button_history release];
    [self.button_setting release];
    [self.scroll1 release];
    [self.scroll2 release];
    [self.page1 release];
    [self.page2 release];
    [self.title1 release];
    [self.title2 release];
    [super dealloc];
}
@end
