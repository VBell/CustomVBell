//
//  HistoryViewController.h
//  linphone
//
//  Created by APPLE on 2014/8/19.
//
//

#ifndef _HistoryViewController_h_
#define _HistoryViewController_h_

#import <UIKit/UIKit.h>

@interface HistoryViewController : UIViewController<UITableViewDelegate, UITableViewDataSource>
{
    NSMutableArray<NSMutableArray<NSMutableDictionary*>*>* tableList;
}
@property (retain, nonatomic) IBOutlet UILabel* top_label;
@property (retain, nonatomic) IBOutlet UIButton* button_back;

@property (retain, nonatomic) IBOutlet UIView* contentArea_Empty;
@property (retain, nonatomic) IBOutlet UILabel* emptyInfo;

@property (retain, nonatomic) IBOutlet UIView* contentArea_List;
@property (retain, nonatomic) IBOutlet UITableView* historyTableView;
@property (retain, nonatomic) IBOutlet UIActivityIndicatorView* loadingIndicator;
@end

#endif
