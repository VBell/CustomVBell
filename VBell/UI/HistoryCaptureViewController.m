//
//  HistoryCaptureViewController.m
//  linphone
//
//  Created by APPLE on 2016/6/22.
//
//

#import "HistoryCaptureViewController.h"
#import "LinphoneAppDelegate.h"
#import <VBellCloud/VBellCloud.h>

@interface HistoryCaptureViewController (Private)
- (void)prepareSnapshotLoading;
@end

@implementation HistoryCaptureViewController (Private)
- (void)prepareSnapshotLoading
{
    dispatch_async(dispatch_get_main_queue(), ^{
        [self.snapshot setImage:[[LinphoneAppDelegate sharedInstance].vbellCloud GetSnapshotFromDevice:[deviceDetail objectForKey:@"mac"] withKey:[deviceDetail objectForKey:@"key"]]];
        [self.snapshot layer].magnificationFilter = kCAFilterNearest;
    });
}
@end

@implementation HistoryCaptureViewController

- (UIStatusBarStyle)preferredStatusBarStyle
{
    return UIStatusBarStyleLightContent;
}

- (BOOL)prefersStatusBarHidden
{
    return YES;
}

#pragma mark - View Init

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    [self.button_back setTitle:NSLocalizedString(BACK, nil) forState:UIControlStateNormal];
    
    NSString* deviceMAC = [deviceDetail objectForKey:@"mac"];
    NSString* deviceName = [deviceDetail objectForKey:@"name"];
    if ([deviceName length] > 0) {
        [self.top_label setText:deviceName];
    } else {
        [self.top_label setText:deviceMAC];
    }
    [self.event_time setText:[NSString stringWithFormat:@"%@ %@", [deviceDetail objectForKey:@"Date"], [deviceDetail objectForKey:@"Time"]]];
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
}

- (void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
    CGRect tmp_frame = [self.timeBar frame];
    if (self.view.frame.size.width >= 375) {
        [self.timeBar setFrame:CGRectMake(tmp_frame.origin.x, tmp_frame.origin.y, tmp_frame.size.width, 40)];
    } else if (self.view.frame.size.width <= 320){
        [self.timeBar setFrame:CGRectMake(tmp_frame.origin.x, tmp_frame.origin.y, tmp_frame.size.width, 30)];
    } else {
        [self.timeBar setFrame:CGRectMake(tmp_frame.origin.x, tmp_frame.origin.y, tmp_frame.size.width, 35)];
    }
}

- (IBAction)onBackClick:(id)sender
{
    [self.navigationController popViewControllerAnimated:YES];
}

- (void)setDeviceDetail:(NSMutableDictionary*)_deatil
{
    if (!_deatil) {
        return;
    }
    deviceDetail = [[NSMutableDictionary alloc] initWithDictionary:_deatil copyItems:YES];
    [self performSelectorInBackground:@selector(prepareSnapshotLoading) withObject:nil];
}

- (void)dealloc {
    [self.top_label release];
    [self.button_back release];
    if (deviceDetail) {
        [deviceDetail release];
    }
    [self.timeBar release];
    [self.snapshot release];
    [self.event_time release];
    [super dealloc];
}
@end
