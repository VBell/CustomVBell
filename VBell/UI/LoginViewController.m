//
//  ViewController.m
//  linphone
//
//  Created by APPLE on 2014/8/19.
//
//

#import "LoginViewController.h"
#import "LinphoneAppDelegate.h"
#import "HomeViewController.h"
#import <VBellCloud/VBellCloud.h>

@interface LoginViewController (Private)
- (void)onRegisterFinish:(NSNotification*)notif;
- (void)onLoginFinish:(NSNotification*)notif;
- (void)onForgotPasswordFinish:(NSNotification*)notif;
- (void)onGetDeviceListFinish:(NSNotification*)notif;

- (void)ResponseHandlerInit;
@end

@implementation LoginViewController (Private)
- (void)onRegisterFinish:(NSNotification*)notif
{
    NSDictionary* resp = [notif userInfo];
    NSString* res_code = [resp objectForKey:@"res_code"];
    NSString* status = [resp objectForKey:@"status"];
    if ([res_code isEqualToString:@"200"]) {
        if ([status isEqualToString:@"C_ERx0000"]) {
            UIAlertView *alert=[[UIAlertView alloc] initWithTitle:NSLocalizedString(SUCCESS, nil) message:[NSString stringWithFormat:@"%@%@",NSLocalizedString(NEWACC, nil),NSLocalizedString(SUCCESS, nil)] delegate:nil cancelButtonTitle:nil otherButtonTitles:NSLocalizedString(OK, nil), nil];
            [alert show];
            [alert release];
            [UserInfo setString:self.text_acc.text forKey:DG_ACC];
            [UserInfo setString:self.text_pwd.text forKey:DG_PWD];
            [self sumit_cancel:self.button_cancel];
        } else {
            UIAlertView *alert=[[UIAlertView alloc] initWithTitle:[resp objectForKey:@"title"] message:[resp objectForKey:@"detail"] delegate:nil cancelButtonTitle:nil otherButtonTitles:NSLocalizedString(OK, nil), nil];
            [alert show];
            [alert release];
        }
    } else {
        UIAlertView *alert=[[UIAlertView alloc] initWithTitle:NSLocalizedString(CON_ERROR_TITLE, nil) message:NSLocalizedString(CON_ERROR_MESSAGE, nil) delegate:nil cancelButtonTitle:nil otherButtonTitles:NSLocalizedString(OK, nil), nil];
        [alert show];
        [alert release];
    }
    [[LinphoneAppDelegate sharedInstance] HideBusyView];
}

- (void)onLoginFinish:(NSNotification*)notif
{
    NSDictionary* resp = [notif userInfo];
    NSString* res_code = [resp objectForKey:@"res_code"];
    NSString* status = [resp objectForKey:@"status"];
    if ([res_code isEqualToString:@"200"]) {
        if ([status isEqualToString:@"C_ERx0000"]) {
            NSLog(@"Login Success.");
            NSString* _cloud_account = [resp objectForKey:@"cloud_account"];
            NSString* _cloud_password = [resp objectForKey:@"cloud_password"];
            [UserInfo setString:_cloud_account forKey:DG_ACC];
            [UserInfo setString:_cloud_password forKey:DG_PWD];
            //wait for auto get device list
            //[[LinphoneAppDelegate sharedInstance].vbellCloud getDeviceListWithTag:0 checkError:YES returnToMainThread:YES];
            return;
        } else {
            UIAlertView* alert = [[UIAlertView alloc] initWithTitle:[resp objectForKey:@"title"] message:[resp objectForKey:@"detail"] delegate:nil cancelButtonTitle:nil otherButtonTitles:NSLocalizedString(OK, nil), nil];
            [alert show];
            [alert release];
        }
    } else {
        UIAlertView* alert = [[UIAlertView alloc] initWithTitle:NSLocalizedString(CON_ERROR_TITLE, nil) message:NSLocalizedString(CON_ERROR_MESSAGE, nil) delegate:nil cancelButtonTitle:nil otherButtonTitles:NSLocalizedString(OK, nil), nil];
        [alert show];
        [alert release];
    }
    [[LinphoneAppDelegate sharedInstance] HideBusyView];
}

- (void)onForgotPasswordFinish:(NSNotification*)notif
{
    NSDictionary* resp = [notif userInfo];
    NSString* res_code = [resp objectForKey:@"res_code"];
    if ([res_code isEqualToString:@"200"]) {
        UIAlertView *alert=[[UIAlertView alloc] initWithTitle:[resp objectForKey:@"title"] message:[resp objectForKey:@"detail"] delegate:nil cancelButtonTitle:nil otherButtonTitles:NSLocalizedString(OK, nil), nil];
        [alert show];
        [alert release];
    } else {
        UIAlertView *alert=[[UIAlertView alloc] initWithTitle:NSLocalizedString(CON_ERROR_TITLE, nil) message:NSLocalizedString(CON_ERROR_MESSAGE, nil) delegate:nil cancelButtonTitle:nil otherButtonTitles:NSLocalizedString(OK, nil), nil];
        [alert show];
        [alert release];
    }
}

- (void)onGetDeviceListFinish:(NSNotification*)notif
{
    NSDictionary* resp = [notif userInfo];
    NSString* res_code = [resp objectForKey:@"res_code"];
    NSString* status = [resp objectForKey:@"status"];
    if ([res_code isEqualToString:@"200"] && [status isEqualToString:@"C_ERx0000"]) {
        [[LinphoneAppDelegate sharedInstance] UpdateDeviceList:[resp objectForKey:@"cam_list"]];
        [[NSNotificationCenter defaultCenter] removeObserver:self];
        [LinphoneAppDelegate sharedInstance].LoginFlag = YES;
        [[LinphoneAppDelegate sharedInstance] UpdatePushNotificationID];
        [[LinphoneAppDelegate sharedInstance] GoTo_HomeViewController];
    } else {
        UIAlertView *alert=[[UIAlertView alloc] initWithTitle:NSLocalizedString(CON_ERROR_TITLE, nil) message:NSLocalizedString(CON_ERROR_MESSAGE, nil) delegate:nil cancelButtonTitle:nil otherButtonTitles:NSLocalizedString(OK, nil), nil];
        [alert show];
        [alert release];
    }
    [[LinphoneAppDelegate sharedInstance] HideBusyView];
}

- (void)ResponseHandlerInit
{
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(onLoginFinish:) name:kLoginResponse object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(onRegisterFinish:) name:kRegisterResponse object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(onForgotPasswordFinish:) name:kForgotPasswordResponse object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(onGetDeviceListFinish:) name:kGetDeviceListResponse object:nil];
}
@end

@implementation LoginViewController

- (UIStatusBarStyle)preferredStatusBarStyle
{
    return UIStatusBarStyleLightContent;
}

- (BOOL)prefersStatusBarHidden
{
    if ([[UserInfo platformString] hasPrefix:@"iPad"])
        return YES;
    return NO;
}

#pragma mark - View Init

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    [LinphoneAppDelegate sharedInstance].Navigation = (CustomNavigation*)self.navigationController;
    [self ResponseHandlerInit];
    [self.text_acc setText:[UserInfo getStringForKey:DG_ACC]];
    [self.text_pwd setText:[UserInfo getStringForKey:DG_PWD]];
    [self.lab_version setText:[NSString stringWithFormat:@"Version %@",[[NSBundle mainBundle] objectForInfoDictionaryKey:@"CFBundleShortVersionString"]]];
    [self.lab_acc setText:NSLocalizedString(ACC, nil)];
    [self.lab_pwd setText:NSLocalizedString(PWD, nil)];
    [self.lab_title_add setText:NSLocalizedString(NEWACC, nil)];
    [self.button_cancel setTitle:NSLocalizedString(CANCEL, nil) forState:UIControlStateNormal];
    [self.button_sumit setTitle:NSLocalizedString(SUBMIT, nil) forState:UIControlStateNormal];
    [self.button_login setTitle:NSLocalizedString(LOGIN, nil) forState:UIControlStateNormal];
    [self.button_register setTitle:NSLocalizedString(NEWACC, nil) forState:UIControlStateNormal];
    [self.button_forget setTitle:NSLocalizedString(FORGET, nil) forState:UIControlStateNormal];
    
    if ([self.text_acc.text length] > 0 && [self.text_pwd.text length] > 0)
    {
        [[LinphoneAppDelegate sharedInstance] ShowBusyViewWithMessage:NSLocalizedString(WAIT, nil)];
        [[LinphoneAppDelegate sharedInstance].vbellCloud loginAccWith:[self.text_acc text] withPassword:[self.text_pwd text] withTag:0 returnToMainThread:YES];
    }
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    count = 0;
    registerFlag = NO;
}

- (void)RefreshLayout
{
    [UIView animateWithDuration:0.7 animations:^(void) {
        if (registerFlag)
        {
            [self.button_cancel setHidden:NO];
            [self.button_sumit setHidden:NO];
            [self.lab_title_add setHidden:NO];
            [self.button_login setAlpha:0];
            [self.button_register setAlpha:0];
            [self.lab_title setAlpha:0];
            [self.button_forget setAlpha:0];
            [self.button_cancel setAlpha:1];
            [self.button_sumit setAlpha:1];
            [self.lab_title_add setAlpha:1];
        }
        else
        {
            [self.button_login setHidden:NO];
            [self.button_register setHidden:NO];
            [self.lab_title setHidden:NO];
            [self.button_forget setHidden:NO];
            [self.button_login setAlpha:1];
            [self.button_register setAlpha:1];
            [self.lab_title setAlpha:1];
            [self.button_forget setAlpha:1];
            [self.button_cancel setAlpha:0];
            [self.button_sumit setAlpha:0];
            [self.lab_title_add setAlpha:0];
        }
    } completion:^(BOOL finish) {
        if (registerFlag)
        {
            [self.button_login setHidden:YES];
            [self.button_register setHidden:YES];
            [self.lab_title setHidden:YES];
            [self.button_forget setHidden:YES];
        }
        else
        {
            [self.button_cancel setHidden:YES];
            [self.button_sumit setHidden:YES];
            [self.lab_title_add setHidden:YES];
        }
    }];
}

#pragma mark - UITextField

- (BOOL)textFieldShouldReturn:(UITextField *)textField
{
    [textField resignFirstResponder];
    return YES;
}

#pragma mark - Button Action

- (IBAction)view_down:(id)sender
{
    [_text_acc resignFirstResponder];
    [_text_pwd resignFirstResponder];
    count++;
    if (count > 20)
        [self.lab_version setText:[UserInfo getStringForKey:PUSHID]];
}

- (IBAction)button_register:(id)sender
{
    registerFlag = YES;
    [self RefreshLayout];
    self.text_acc.text = @"";
    self.text_pwd.text = @"";
}

- (IBAction)Login:(id)sender
{
    if (![self.text_acc.text isEqualToString:@""] && ![self.text_pwd.text isEqualToString:@""] && [[self.text_acc.text stringByReplacingOccurrencesOfString:@" " withString:@""] length] > 0 && [[self.text_pwd.text stringByReplacingOccurrencesOfString:@" " withString:@""] length] > 0)
    {
        [[LinphoneAppDelegate sharedInstance] ShowBusyViewWithMessage:NSLocalizedString(WAIT, nil)];
        [[LinphoneAppDelegate sharedInstance].vbellCloud loginAccWith:[self.text_acc text] withPassword:[self.text_pwd text] withTag:0 returnToMainThread:YES];
    }
    else
    {
        UIAlertView *alert=[[UIAlertView alloc] initWithTitle:NSLocalizedString(ERROR_TITLE, nil) message:NSLocalizedString(ERROR_ACCPW, nil) delegate:nil cancelButtonTitle:nil otherButtonTitles:NSLocalizedString(OK, nil), nil];
        [alert show];
        [alert release];
    }
}

- (IBAction)Forget:(id)sender
{
    [self.text_acc resignFirstResponder];
    [self.text_pwd resignFirstResponder];
    UIAlertView *alert=[[UIAlertView alloc] initWithTitle:NSLocalizedString(FORGET, nil) message:NSLocalizedString(INPUT_MAIL, nil) delegate:self cancelButtonTitle:NSLocalizedString(CANCEL, nil) otherButtonTitles:NSLocalizedString(OK, nil), nil];
    [alert setAlertViewStyle:UIAlertViewStylePlainTextInput];
    [alert show];
    [alert release];
}

- (IBAction)sumit_cancel:(UIButton *)sender
{
    if (sender == self.button_sumit)
    {
        if (![self.text_acc.text isEqualToString:@""] && ![self.text_pwd.text isEqualToString:@""] && [[self.text_acc.text stringByReplacingOccurrencesOfString:@" " withString:@""] length] > 0 && [[self.text_pwd.text stringByReplacingOccurrencesOfString:@" " withString:@""] length] > 0)
        {
            if (![VBell_Cloud NSStringIsValidEmail:[self.text_acc text]]) {
                UIAlertView* alert = [[UIAlertView alloc] initWithTitle:NSLocalizedString(ERROR_TITLE, nil) message:NSLocalizedString(LOGIN_ERROR_ACC_FORMAT, nil) delegate:nil cancelButtonTitle:nil otherButtonTitles:NSLocalizedString(OK, nil), nil];
                [alert show];
                [alert release];
                return;
            }
            [[LinphoneAppDelegate sharedInstance] ShowBusyViewWithMessage:NSLocalizedString(WAIT, nil)];
            [[LinphoneAppDelegate sharedInstance].vbellCloud registerAccount:[self.text_acc text] withPassword:[self.text_pwd text] withTag:0 returnToMainThread:YES];
        }
        else
        {
            UIAlertView* alert = [[UIAlertView alloc] initWithTitle:NSLocalizedString(ERROR_TITLE, nil) message:NSLocalizedString(ERROR_ACCPW, nil) delegate:nil cancelButtonTitle:nil otherButtonTitles:NSLocalizedString(OK, nil), nil];
            [alert show];
            [alert release];
        }
    }
    else
    {
        registerFlag = NO;
        [self RefreshLayout];
        [self.text_acc setText:[UserInfo getStringForKey:DG_ACC]];
        [self.text_pwd setText:[UserInfo getStringForKey:DG_PWD]];
    }
}

#pragma mark - UIAlertDelegate

- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    if (buttonIndex==1)
        [[LinphoneAppDelegate sharedInstance].vbellCloud forgotPasswordFor:[alertView textFieldAtIndex:0].text withTag:0 returnToMainThread:YES];
}

- (void)dealloc {
    [[NSNotificationCenter defaultCenter] removeObserver:self];
    [self.text_acc release];
    [self.text_pwd release];
    [self.lab_title release];
    [self.button_login release];
    [self.button_register release];
    [self.button_sumit release];
    [self.button_cancel release];
    [self.lab_title_add release];
    [self.button_forget release];
    [self.lab_version release];
    [self.lab_acc release];
    [self.lab_pwd release];
    [super dealloc];
}
@end
