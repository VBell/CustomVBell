//
//  CallViewController.m
//  linphone
//
//  Created by APPLE on 2014/7/14.
//
//

#import "CallViewController.h"
#import "LinphoneAppDelegate.h"

@interface CallViewController (Private)
- (NSString*)getDetailFromCode:(NSInteger)_code default:(NSString*)_defaultInfo;
@end

@implementation CallViewController (Private)
- (NSString*)getDetailFromCode:(NSInteger)_code default:(NSString*)_defaultInfo
{
    switch (_code) {
        case 403:
            return NSLocalizedString(CallViewController_403, _defaultInfo);
        case 404:
            return NSLocalizedString(CallViewController_404, _defaultInfo);
        case 408:
            return NSLocalizedString(CallViewController_408, _defaultInfo);
        case 480:
            return NSLocalizedString(CallViewController_480, _defaultInfo);
        case 486:
            return NSLocalizedString(CallViewController_486, _defaultInfo);
        default:
            break;
    }
    return _defaultInfo;
}
@end

@implementation CallViewController
@synthesize videoView;
@synthesize image_preview;
@synthesize videoGroup;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        isWallPad = NO;
    }
    return self;
}

#pragma mark - StatusBar

- (UIStatusBarStyle)preferredStatusBarStyle
{
    return UIStatusBarStyleLightContent;
}

- (BOOL)prefersStatusBarHidden
{
    return YES;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    self.open_door.layer.cornerRadius = 8;
    self.decliented.layer.cornerRadius = 8;
    self.accepted.layer.cornerRadius = 8;
    [self.open_door setTitle:NSLocalizedString(OPEN_DOOR, nil) forState:UIControlStateNormal];
    [self.accepted setTitle:NSLocalizedString(ACCEPT, nil) forState:UIControlStateNormal];
    [self.decliented setTitle:NSLocalizedString(HANGUP, nil) forState:UIControlStateNormal];
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    [self setViewSetting];
}

- (void)setViewSetting
{
    [[LinphoneAppDelegate sharedInstance].vbellCloud setNativeVideoWindowID:(void*)videoView];
    if (!videoZoomHandler) {
        videoZoomHandler = [[VideoZoomHandler alloc] init];
        [videoZoomHandler setup:videoView];
    }
}
- (IBAction)call_down:(id)sender
{
    UIButton *bt=sender;
    if (bt.tag == 0) {
        [[LinphoneAppDelegate sharedInstance].vbellCloud AnswerCall];
    } else if (bt.tag == 1) {
        [[LinphoneAppDelegate sharedInstance].vbellCloud HangupCall:isWallPad];
    } else
        [[LinphoneAppDelegate sharedInstance].vbellCloud DoorUnlock];
}

- (void)DismissView
{
    if (prepareDismiss)
        return;
    prepareDismiss = YES;
    [self dismissViewControllerAnimated:NO completion:^(void){}];
    [[LinphoneAppDelegate sharedInstance].vbellCloud HangupCall:[model isEqualToString:@"WP101"]];
    [NSObject cancelPreviousPerformRequestsWithTarget:self selector:@selector(call_down:) object:_decliented];
    prepareDismiss = NO;
}

- (void)dealloc
{
    [[NSNotificationCenter defaultCenter] removeObserver:self];
    [self.open_door release];
    [self.accepted release];
    [self.decliented release];
    [self.caller_id release];
    [self.image_flag release];
    [self.image_preview release];
    if (name)
        [name release];
    if (mac)
        [mac release];
    if (model)
        [model release];
    [videoZoomHandler release];
    [super dealloc];
}

- (void)ChangeModeTo:(CallViewMode)new_mode
{
    switch (new_mode) {
        case CallViewMode_Incoming:
        {
            [self.accepted setHidden:NO];
            if (!isWallPad) {
                [self.image_preview setHidden:NO];
                [self.image_preview startImageDownload_big:mac];
            } else
                [self.image_preview setHidden:YES];
            [self.open_door setHidden:YES];
            [self.decliented setHidden:NO];
            if ([name length] > 0) {
                [self.caller_id setText:name];
            } else
                [self.caller_id setText:mac];
            [[LinphoneAppDelegate sharedInstance] PlayRingTone];
            [self performSelector:@selector(call_down:) withObject:self.decliented afterDelay:30];
            break;
        }
        case CallViewMode_Outgoing:
        {
            [self.accepted setHidden:YES];
            [self.image_preview setHidden:YES];
            [self.open_door setHidden:YES];
            [self.decliented setHidden:NO];
            if ([name length] > 0) {
                [self.caller_id setText:name];
            } else
                [self.caller_id setText:mac];
            break;
        }
        case CallViewMode_Answered:
        {
            UIAlertView *alert=[[UIAlertView alloc] initWithTitle:@"" message:NSLocalizedString(CALLHASBEENANSWER, nil) delegate:nil cancelButtonTitle:NSLocalizedString(SURE, nil) otherButtonTitles:nil, nil];
            [alert show];
            [alert release];
            [[LinphoneAppDelegate sharedInstance] StopRingTone];
            [self performSelectorOnMainThread:@selector(DismissView) withObject:nil waitUntilDone:NO];
            break;
        }
        case CallViewMode_Hangup:
        {
            [[LinphoneAppDelegate sharedInstance] StopRingTone];
            [self performSelectorOnMainThread:@selector(DismissView) withObject:nil waitUntilDone:NO];
            break;
        }
        case CallViewMode_Connected:
        {
            [self.accepted setHidden:YES];
            [self.image_preview setHidden:YES];
            if ([model isEqualToString:@"WP101"] || [model isEqualToString:@"DP107"]) {
                [self.open_door setHidden:YES];
            } else
                [self.open_door setHidden:NO];
            [self.decliented setHidden:NO];
            [[LinphoneAppDelegate sharedInstance] StopRingTone];
            [NSObject cancelPreviousPerformRequestsWithTarget:self selector:@selector(call_down:) object:_decliented];
            break;
        }
        case CallViewMode_None:
        {
            [[LinphoneAppDelegate sharedInstance] StopRingTone];
            [self performSelectorOnMainThread:@selector(DismissView) withObject:nil waitUntilDone:NO];
            break;
        }
        default:
            return;
    }
    current_mode = new_mode;
}

- (void)InitWithName:(NSString*)_name forMAC:(NSString*)_mac forModel:(NSString*)_model
{
    if (name) {
        [name release];
        name = NULL;
    }
    if (mac) {
        [mac release];
        mac = NULL;
    }
    if (model) {
        [model release];
        model = NULL;
    }
    name = [[NSString alloc] initWithString:_name];
    mac = [[NSString alloc] initWithString:_mac];
    model = [[NSString alloc] initWithString:_model];
    isWallPad = [model isEqualToString:@"WP101"];
}

@end
