//
//  ViewController.h
//  linphone
//
//  Created by APPLE on 2014/8/19.
//
//

#ifndef _LoginViewController_h_
#define _LoginViewController_h_

#import <UIKit/UIKit.h>

@interface LoginViewController : UIViewController<UITextFieldDelegate,UIAlertViewDelegate>
{
    BOOL registerFlag;
    int count;
}
@property (retain, nonatomic) IBOutlet UITextField *text_acc;
@property (retain, nonatomic) IBOutlet UITextField *text_pwd;
@property (retain, nonatomic) IBOutlet UILabel *lab_title;
@property (retain, nonatomic) IBOutlet UILabel *lab_title_add;
@property (retain, nonatomic) IBOutlet UIButton *button_login;
@property (retain, nonatomic) IBOutlet UIButton *button_register;
@property (retain, nonatomic) IBOutlet UIButton *button_sumit;
@property (retain, nonatomic) IBOutlet UIButton *button_cancel;
@property (retain, nonatomic) IBOutlet UIButton *button_forget;
@property (retain, nonatomic) IBOutlet UILabel *lab_version;
@property (retain, nonatomic) IBOutlet UILabel *lab_acc;
@property (retain, nonatomic) IBOutlet UILabel *lab_pwd;
- (IBAction)sumit_cancel:(UIButton *)sender;
@end

#endif
