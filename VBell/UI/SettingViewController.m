//
//  SettingViewController.m
//  linphone
//
//  Created by APPLE on 2014/12/5.
//
//

#import "SettingViewController.h"
#import "LinphoneAppDelegate.h"
#import <VBellCloud/VBellCloud.h>

@interface SettingViewController (Private)
- (void)startLoading;
- (void)stopLoading;
- (void)stopLoadingWithErrorInfo:(NSString*)error_information;

- (void)onFinishLoad:(NSNotification*)notif;
- (void)onFinishUpdate:(NSNotification*)notif;

- (void)LoadQRcode;
- (void)ResponseHandlerInit;
@end

@implementation SettingViewController (Private)
- (void)startLoading
{
    [self.network_status setHidden:YES];
    [self.sw setEnabled:NO];
    [self.loading_indicator startAnimating];
}

- (void)stopLoading
{
    [self.sw setEnabled:YES];
    [self.loading_indicator stopAnimating];
}

- (void)stopLoadingWithErrorInfo:(NSString*)error_information
{
    [self.loading_indicator stopAnimating];
    [self.network_status setHidden:NO];
    [self.network_status setText:[NSString stringWithString:error_information]];
}

- (void)onFinishLoad:(NSNotification*)notif
{
    NSDictionary* resp = [notif userInfo];
    NSString* res_code = [resp objectForKey:@"res_code"];
    NSString* status = [resp objectForKey:@"status"];
    if ([res_code isEqualToString:@"200"]) {
        if ([status isEqualToString:@"C_ERx0000"]) {
            NSString* enable = [resp objectForKey:@"enabled"];
            [self.sw setOn:![enable boolValue] animated:YES];
            [UserInfo setBoolean:![enable boolValue] forKey:DISTURB];
        }
        [self stopLoading];
    } else
        [self stopLoadingWithErrorInfo:NSLocalizedString(NETWORK_STATUS_UNAVAILABLE, nil)];
}

- (void)onFinishUpdate:(NSNotification*)notif
{
    NSDictionary* resp = [notif userInfo];
    NSString* res_code = [resp objectForKey:@"res_code"];
    NSString* status = [resp objectForKey:@"status"];
    if ([res_code isEqualToString:@"200"]) {
        if ([status isEqualToString:@"C_ERx0000"]) {
            [UserInfo setBoolean:self.sw.isOn forKey:DISTURB];
        }
        [self stopLoading];
    } else {
        [self stopLoadingWithErrorInfo:NSLocalizedString(NETWORK_STATUS_UNAVAILABLE, nil)];
        [self.sw setOn:!self.sw.isOn animated:YES];
    }
}

- (void)LoadQRcode
{
    dispatch_async(dispatch_get_main_queue(), ^{
        [self.qrcode_image setImage:[[LinphoneAppDelegate sharedInstance].vbellCloud GetAccountQRCode]];
        [self.qrcode_image layer].magnificationFilter = kCAFilterNearest;
    });
    /*NSString *_imgURL = [NSString stringWithFormat:@"http://vbell.myehome.my:81/qrcode.php?content=%@", self.accountLabel.text];
    NSData *_data = [NSData dataWithContentsOfURL:[NSURL URLWithString:_imgURL]];
    //set your image on main thread.
    dispatch_async(dispatch_get_main_queue(), ^{
        UIImage* image = nil;
        if (_data == nil) {
            image = [QREncoder encode:self.accountLabel.text];
        } else
            image = [UIImage imageWithData:_data];
        [self.qrcode_image setImage:image];
        [self.qrcode_image layer].magnificationFilter = kCAFilterNearest;
    });*/
}

- (void)ResponseHandlerInit
{
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(onFinishLoad:) name:kGetAlarmResponse object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(onFinishUpdate:) name:kSetAlarmResponse object:nil];
}
@end

@interface SettingViewController ()

@end

@implementation SettingViewController

- (UIStatusBarStyle)preferredStatusBarStyle
{
    return UIStatusBarStyleLightContent;
}

- (BOOL)prefersStatusBarHidden
{
    return YES;
}

#pragma mark - View Init

- (void)viewDidLoad
{
    [super viewDidLoad];
    [self ResponseHandlerInit];
    [self.lab_version setText:[NSString stringWithFormat:@"Version %@", [[NSBundle mainBundle] objectForInfoDictionaryKey:@"CFBundleShortVersionString"]]];
    [self.lab setText:NSLocalizedString(NOT_DISTURB, nil)];
    [self.content setText:NSLocalizedString(DISTURB_INFO, nil)];
    [self.button_back setTitle:NSLocalizedString(BACK, nil) forState:UIControlStateNormal];
    [self.lab_top setText:NSLocalizedString(SETTING, nil)];
    [self.accountLabel setText:[UserInfo getStringForKey:DG_ACC]];
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    [[LinphoneAppDelegate sharedInstance].vbellCloud getAlarmWithTag:0 returnToMainThread:YES];
    [self startLoading];
    [self LoadQRcode];
}

- (IBAction)back_down:(id)sender
{
    [self.navigationController popViewControllerAnimated:YES];
}

- (IBAction)sw_down:(id)sender
{
    UISwitch* sw = sender;
    [[LinphoneAppDelegate sharedInstance].vbellCloud setAlarm:sw.isOn WithTag:0 returnToMainThread:YES];
    [self startLoading];
}

- (void)dealloc {
    [[NSNotificationCenter defaultCenter] removeObserver:self];
    [self.lab_version release];
    [self.lab release];
    [self.sw release];
    [self.content release];
    [self.button_back release];
    [self.lab_top release];
    [self.network_status release];
    [self.loading_indicator release];
    [self.qrcode_image release];
    [self.accountLabel release];
    [super dealloc];
}
@end
