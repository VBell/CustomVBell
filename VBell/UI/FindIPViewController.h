//
//  FindIPViewController.h
//  CustomVBell
//
//  Created by APPLE on 2016/7/5.
//
//

#ifndef _FindIPViewController_h_
#define _FindIPViewController_h_

#import <UIKit/UIKit.h>
#import "LinphoneAppDelegate.h"
#import <VBellCloud/VBellCloud.h>

@interface FindIPViewController : UIViewController<ScanDP> {
    ScanDP *sdp;
    NSString* find_target;
}
@property(nonatomic, retain) IBOutlet UILabel* topLabel;
@property(nonatomic, retain) IBOutlet UIButton* backButton;
@property(nonatomic, retain) IBOutlet UILabel* deviceLabel;
@property(nonatomic, retain) IBOutlet UILabel* deviceMAC;
@property(nonatomic, retain) IBOutlet UILabel* ipLabel;
@property(nonatomic, retain) IBOutlet UILabel* ipAddress;
@property(nonatomic, retain) IBOutlet UIActivityIndicatorView* loadingIndicator;

- (void)setFindedDevice:(NSString*)_device_mac;
@end

#endif
