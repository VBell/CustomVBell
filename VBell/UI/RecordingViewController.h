//
//  RecordingViewController.h
//  CustomVBell
//
//  Created by APPLE on 2016/7/5.
//
//

#ifndef _RecordingViewController_h_
#define _RecordingViewController_h_

#import <UIKit/UIKit.h>
#import <VBellCloud/VBellCloud.h>

@interface RecordingViewController : UIViewController<ScanDP> {
    ScanDP *sdp;
    NSString* find_target;
}
@property(nonatomic, retain) IBOutlet UILabel* topLabel;
@property(nonatomic, retain) IBOutlet UIButton* backButton;
@property(nonatomic, retain) IBOutlet UIActivityIndicatorView* loadingIndicator;
@property(nonatomic, retain) IBOutlet UILabel* loadingLabel;
@property(nonatomic, retain) IBOutlet UIWebView* recordingPage;

- (void)setFindedDevice:(NSString*)_device_mac;
@end

#endif
