//
//  SettingViewController.h
//  linphone
//
//  Created by APPLE on 2014/12/5.
//
//

#ifndef _SettingViewController_h_
#define _SettingViewController_h_

#import <UIKit/UIKit.h>
#import <VBellCloud/VBellCloud.h>

@interface SettingViewController : UIViewController {
}

@property (retain, nonatomic) IBOutlet UILabel *lab_version;
@property (retain, nonatomic) IBOutlet UILabel *lab;
@property (retain, nonatomic) IBOutlet UISwitch *sw;
@property (retain, nonatomic) IBOutlet UILabel *content;
@property (retain, nonatomic) IBOutlet UIButton *button_back;
@property (retain, nonatomic) IBOutlet UILabel *lab_top;
@property (retain, nonatomic) IBOutlet UILabel *network_status;
@property (retain, nonatomic) IBOutlet UIActivityIndicatorView *loading_indicator;
@property (retain, nonatomic) IBOutlet UIImageView *qrcode_image;
@property (retain, nonatomic) IBOutlet UILabel *accountLabel;
@end

#endif
