/* LinphoneAppDelegate.m
 *
 * Copyright (C) 2009  Belledonne Comunications, Grenoble, France
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 */
#import "linphoneAppDelegate.h"

#import "CoreTelephony/CTCallCenter.h"
#import "CoreTelephony/CTCall.h"

@interface LinphoneAppDelegate (Private)
- (void)onUpdatePushIDFinish:(NSNotification*)notif;

- (void)InitVBellCloud;
- (void)handleCallUpdateEvent:(NSNotification*)notif;
@end

@implementation LinphoneAppDelegate (Private)

- (void)onUpdatePushIDFinish:(NSNotification*)notif
{
    NSDictionary* resp = [notif userInfo];
    NSString* res_code = [resp objectForKey:@"res_code"];
    if ([res_code isEqualToString:@"200"]) {
    } else {}
}

- (void)onUpdatePushIDError:(NSObject*)data
{
    CURL_HttpsConnectionResponse* resData = (CURL_HttpsConnectionResponse*)data;
    NSLog(@"onUpdatePushIDError: resData.resCode=%@, resData.resMessage=%@", resData.resCode, resData.resMessage);
}

- (void)InitVBellCloud
{
    vbellCloud = [[VBell_Cloud alloc] init];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(handleCallUpdateEvent:) name:kCallUpdateEvent object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(onUpdatePushIDFinish:) name:kUpdatePushIDResponse object:nil];
}

- (void)handleCallUpdateEvent:(NSNotification*)notif
{
    NSDictionary* dict = [notif userInfo];
    VBellCloud_CallUpdateEvent event = [[dict objectForKey:@"event"] intValue];
    NSLog(@"VBellCloud_CallUpdateEvent=%d", event);
    switch (event) {
        case VBellCloud_CallUpdateEvent_Incoming:
        {
            NSString* _mac = [dict objectForKey:@"mac"];
            NSString* _name = [dict objectForKey:@"name"];
            NSString* _model = [dict objectForKey:@"model"];
            if (incomming_device) {
                [incomming_device release];
                incomming_device = NULL;
            }
            incomming_device = [[NSString alloc] initWithString:_mac];
            [callViewController InitWithName:_name forMAC:_mac forModel:_model];
            [self.Navigation presentViewController:callViewController animated:YES completion:^(void){
            }];
            [callViewController ChangeModeTo:CallViewMode_Incoming];
            break;
        }
        case VBellCloud_CallUpdateEvent_Hangup:
        {
            if (incomming_device) {
                [incomming_device release];
                incomming_device = NULL;
            }
            [callViewController ChangeModeTo:CallViewMode_Hangup];
            break;
        }
        case VBellCloud_CallUpdateEvent_Answered:
        {
            NSString* _mac = [dict objectForKey:@"mac"];
            if (incomming_device && [incomming_device isEqualToString:_mac]) {
                [callViewController ChangeModeTo:CallViewMode_Answered];
            }
            break;
        }
        case VBellCloud_CallUpdateEvent_Connected:
        {
            if (incomming_device) {
                [incomming_device release];
                incomming_device = NULL;
            }
            [callViewController ChangeModeTo:CallViewMode_Connected];
            break;
        }
        case VBellCloud_CallUpdateEvent_Outgoing:
        {
            NSString* _mac = [dict objectForKey:@"mac"];
            NSString* _name = [dict objectForKey:@"name"];
            NSString* _model = [dict objectForKey:@"model"];
            [callViewController InitWithName:_name forMAC:_mac forModel:_model];
            [self.Navigation presentViewController:callViewController animated:YES completion:^(void){
            }];
            [callViewController ChangeModeTo:CallViewMode_Outgoing];
            break;
        }
        case VBellCloud_CallUpdateEvent_End:
        {
            if (incomming_device) {
                [incomming_device release];
                incomming_device = NULL;
            }
            [callViewController ChangeModeTo:CallViewMode_Hangup];
            break;
        }
        case VBellCloud_CallUpdateEvent_Error:
        {
            if (incomming_device) {
                [incomming_device release];
                incomming_device = NULL;
            }
            [callViewController ChangeModeTo:CallViewMode_Hangup];
            break;
        }
        default:
            break;
    }
}
@end

@implementation LinphoneAppDelegate
@synthesize configURL;
@synthesize window;
@synthesize LoginFlag;
@synthesize vbellCloud;

#pragma mark - Lifecycle Functions

- (id)init {
    self = [super init];
    if(self != nil) {
        self->startedInBackground = FALSE;
    }
    return self;
}

- (void)dealloc {
    if (callViewController)
        [callViewController release];
    if (homeViewController)
        [homeViewController release];
    [_Navigation release];
    [vbellCloud release];
    [super dealloc];
}

- (void)applicationDidEnterBackground:(UIApplication *)application
{
    [vbellCloud ApplicationDidEnterBackground];
}

- (void)applicationWillResignActive:(UIApplication *)application
{
    [vbellCloud ApplicationWillResignActive];
}

- (void)applicationDidBecomeActive:(UIApplication *)application
{
    if( startedInBackground ){
        startedInBackground = FALSE;
    }
    [vbellCloud ApplicationDidBecomeActive];
}

- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions {
    UIApplication* app = [UIApplication sharedApplication];
    if( [app respondsToSelector:@selector(registerUserNotificationSettings:)] ) {
        UIUserNotificationType notifTypes = UIUserNotificationTypeBadge|UIUserNotificationTypeSound|UIUserNotificationTypeAlert;
        UIUserNotificationSettings* userSettings = [UIUserNotificationSettings settingsForTypes:notifTypes categories:nil];
        [app registerUserNotificationSettings:userSettings];
        [app registerForRemoteNotifications];
    }
    bgStartId = [[UIApplication sharedApplication] beginBackgroundTaskWithExpirationHandler:^{
        [[UIApplication sharedApplication] endBackgroundTask:bgStartId];
    }];
    [self InitVBellCloud];
    // initialize UI
    [self.window makeKeyAndVisible];
    [self startApplication];
    NSDictionary *remoteNotif =[launchOptions objectForKey:UIApplicationLaunchOptionsRemoteNotificationKey];
    if (remoteNotif) {
        [self processRemoteNotification:remoteNotif];
    }
    if (bgStartId!=UIBackgroundTaskInvalid) [[UIApplication sharedApplication] endBackgroundTask:bgStartId];
    [UserInfo setString:@"" forKey:PUSHID];
    return YES;
}

- (void)startApplication
{
    if (!callViewController) {
        callViewController = [[CallViewController alloc] initWithNibName: @"CallViewController" bundle:nil];
    }
    if (!homeViewController) {
        homeViewController = [[HomeViewController alloc] initWithNibName:@"HomeViewController" bundle:nil];
    }
}

- (void)fixRing {
    if ([[[UIDevice currentDevice] systemVersion] floatValue] >= 7) {
        // iOS7 fix for notification sound not stopping.
        // see http://stackoverflow.com/questions/19124882/stopping-ios-7-remote-notification-sound
        [[UIApplication sharedApplication] setApplicationIconBadgeNumber: 1];
        [[UIApplication sharedApplication] setApplicationIconBadgeNumber: 0];
    }
}

- (void)processRemoteNotification:(NSDictionary*)userInfo
{
    NSLog(@"%@",userInfo);
    if (![UserInfo getBooleanForKey:DISTURB])
    {
        [vbellCloud processRemoteNotification:userInfo];
    }
}

- (void)application:(UIApplication *)application didReceiveRemoteNotification:(NSDictionary *)userInfo
{
    [self processRemoteNotification:userInfo];
}

- (void)application:(UIApplication *)application didReceiveLocalNotification:(UILocalNotification *)notification
{
    [self fixRing];
}

#pragma mark - PushNotification Functions

- (void)application:(UIApplication*)application didRegisterForRemoteNotificationsWithDeviceToken:(NSData*)deviceToken
{
    NSString *device_token=[deviceToken description];
    device_token = [device_token stringByReplacingOccurrencesOfString:@">" withString:@""];
    device_token = [device_token stringByReplacingOccurrencesOfString:@"<" withString:@""];
    
    [UserInfo setString:[device_token stringByReplacingOccurrencesOfString:@" " withString:@""] forKey:PUSHID];
    NSLog(@"device_token=%@",device_token);
    if (LoginFlag == YES) {
        [self UpdatePushNotificationID];
    }
}

- (void)application:(UIApplication*)application didFailToRegisterForRemoteNotificationsWithError:(NSError*)error
{
    [UserInfo setString:@"" forKey:PUSHID];
}

#pragma mark - User notifications

- (void)application:(UIApplication *)application handleActionWithIdentifier:(NSString *)identifier forLocalNotification:(UILocalNotification *)notification completionHandler:(void (^)())completionHandler {
    completionHandler();
}

- (void)application:(UIApplication *)application handleActionWithIdentifier:(NSString *)identifier forRemoteNotification:(NSDictionary *)userInfo completionHandler:(void (^)())completionHandler {
    completionHandler();
}

+(LinphoneAppDelegate*) sharedInstance
{
    return ((LinphoneAppDelegate*) [[UIApplication sharedApplication] delegate]);
}

#pragma mark - PlayRingTone

-(void)PlayRingTone
{
    if (playerRingTone==nil)
    {
        NSURL *ring_name =[NSURL fileURLWithPath:[NSString stringWithFormat:@"%@/%@", [[NSBundle mainBundle] resourcePath], [NSString stringWithFormat:@"ring.caf"]]];
        NSLog(@"Ring=%@",ring_name);
        [playerRingTone stop];
        playerRingTone =[[AVAudioPlayer alloc] initWithContentsOfURL:ring_name error:nil];
        playerRingTone.numberOfLoops=-1;
        [playerRingTone setVolume:1];
        [playerRingTone play];
    }
}

-(void)StopRingTone
{
    if (playerRingTone != nil)
    {
        [playerRingTone stop];
        [playerRingTone release];
        playerRingTone = nil;
    }
}

- (void) UpdatePushNotificationID
{
    NSString* _push_id = [UserInfo getStringForKey:PUSHID];
    if (_push_id && [_push_id length] > 0) {
        [vbellCloud updatePushID:_push_id WithTag:0 returnToMainThread:NO];
    }
}

-(void)UpdateDeviceList:(NSMutableArray<NSMutableArray<NSMutableDictionary*>*>*) _deviceList
{
    if (!_deviceList)
        return;
    if (deviceList) {
        [deviceList release];
    }
    deviceList = [[NSMutableArray<NSMutableArray<NSMutableDictionary*>*> alloc] initWithArray:_deviceList copyItems:YES];
}

-(NSMutableArray<NSMutableDictionary*>*)GetOwnDeviceList
{
    if (!deviceList)
        return NULL;
    return [deviceList objectAtIndex:0];
}

-(NSMutableArray<NSMutableDictionary*>*)GetViewDeviceList
{
    if (!deviceList)
        return NULL;
    return [deviceList objectAtIndex:1];
}

-(void)ShowBusyViewWithMessage:(NSString*)_message
{
    [BusyView addBusyViewFromView:self.Navigation.view message:_message];
}

-(void)HideBusyView
{
    [BusyView removeBusyViewFromView:self.Navigation.view];
}

-(void)GoTo_HomeViewController
{
    [self.Navigation pushViewController:homeViewController animated:YES];
}
@end
