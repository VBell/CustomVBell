//
//  DoorGuard_Util.h
//  linphone
//
//  Created by APPLE on 2014/10/17.
//
//
#ifndef _DoorGuard_Util_h
#define _DoorGuard_Util_h

#define DG_ACC                  @"DG_ACC"
#define DG_PWD                  @"DG_PWD"
#define PUSHID                  @"PUSHID"
#define HELP                    @"HELP"
#define LANG                    @"LANG"
#define ACC                     @"ACC"
#define PWD                     @"PWD"
#define FORGET                  @"FORGET"
#define LOGIN                   @"LOGIN"
#define LOGOUT                  @"LOGOUT"
#define REGISTER                @"REGISTER"
#define NEWACC                  @"NEWACC"
#define DELETE                  @"DELETE"
#define SUBMIT                  @"SUBMIT"
#define CANCEL                  @"CANCEL"
#define CLEAR                   @"CLEAR"
#define OK                      @"OK"
#define FINISH                  @"FINISH"
#define SCAN                    @"SCAN"
#define BACK                    @"BACK"
#define NEXT                    @"NEXT"
#define MYQR                    @"MYQR"
#define ADDDP                   @"ADDDP"
#define ERROR_TITLE             @"ERROR_TITLE"
#define ERROR_ACCPW             @"ERROR_ACCPW"
#define EMPTY                   @"EMPTY"
#define EMPTY_MAIL              @"EMPTY_MAIL"
#define EMPTY_NAME              @"EMPTY_NAME"
#define EMPTY_ACC               @"EMPTY_ACC"
#define INPUT_MAIL              @"INPUT_MAIL"
#define WAIT                    @"WAIT"
#define CONNECTING              @"CONNECTING"
#define LOADING                 @"LOADING"
#define DELETING                @"DELETING"
#define SAVING                  @"SAVING"
#define ADDING                  @"ADDING"
#define SUCCESS                 @"SUCCESS"
#define ADD                     @"ADD"
#define CON_ERROR_TITLE         @"CON_ERROR_TITLE"
#define CON_ERROR_MESSAGE       @"CON_ERROR_MESSAGE"
#define CHANGE_DP_NAME          @"CHANGE_DP_NAME"
#define SHARE                   @"SHARE"
#define DP_OFFLINE              @"DP_OFFLINE"
#define MY                      @"MY"
#define NODP_ADDDP              @"NODP_ADDDP"
#define CHANGE_DP_NAME_S        @"CHANGE_DP_NAME_S"
#define INPUT_NEW_NAME          @"INPUT_NEW_NAME"
#define SURE                    @"SURE"
#define SHARER                  @"SHARER"
#define INPUT_ACC_SHARE         @"INPUT_ACC_SHARE"
#define OPEN_DOOR               @"OPEN_DOOR"
#define ACCEPT                  @"ACCEPT"
#define HANGUP                  @"HANGUP"
#define SETTING                 @"SETTING"
#define SCAN_OR_INPUT           @"SCAN_OR_INPUT"
#define SHARE_TO_ME             @"SHARE_TO_ME"
#define RECORDING               @"RECORDING"
#define FIND_IP                 @"FIND_IP"
#define WIFI_INFO1              @"WIFI_INFO1"
#define WIFI_INFO2              @"WIFI_INFO2"
#define WIFI_NAME               @"ssid"
#define WIFI_AUTH               @"authentication"
#define WIFI_ENCRY              @"encryption"
#define WIFI_KEY                @"key"
#define CALLHASBEENANSWER       @"CALLHASBEENANSWER"
#define SHOW                    @"SHOW"
#define NOT_DISTURB             @"NOT_DISTURB"
#define DISTURB_INFO            @"DISTURB_INFO"

#pragma mark - SettingViewController
#define NETWORK_STATUS_UNAVAILABLE          @"NETWORK_STATUS_UNAVAILABLE"
#define DISTURB                             @"DISTURB"

#pragma mark - LoginViewController
#define LOGIN_ERROR_ACC_FORMAT              @"LOGIN_ERROR_ACC_FORMAT"

#pragma mark - HomeViewController
#define HomeViewController_HistoryLabel     @"HomeViewController_HistoryLabel"

#pragma mark - HistoryViewCell
#define HistoryViewCell_DetailInfo_CALL     @"HistoryViewCell_DetailInfo_CALL"
#define HistoryViewCell_DetailInfo_ANSWER   @"HistoryViewCell_DetailInfo_ANSWER"
#define HistoryViewCell_DetailInfo_MOTION   @"HistoryViewCell_DetailInfo_MOTION"

#pragma mark - FindIPViewController
#define FindIPViewController_DeviceLabel    @"FindIPViewController_DeviceLabel"
#define FindIPViewController_NotFound       @"FindIPViewController_NotFound"

#pragma mark - RecordingViewController
#define RecordingViewController_LoadingLabel @"RecordingViewController_LoadingLabel"

#pragma mark - CallViewController
#define CallViewController_403 @"CallViewController_403"
#define CallViewController_404 @"CallViewController_404"
#define CallViewController_408 @"CallViewController_408"
#define CallViewController_480 @"CallViewController_480"
#define CallViewController_486 @"CallViewController_486"
#endif
