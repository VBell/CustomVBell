/* LinphoneAppDelegate.h
 *
 * Copyright (C) 2009  Belledonne Comunications, Grenoble, France
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 */

#ifndef _LinphoneAppDelegate_h_
#define _LinphoneAppDelegate_h_

#import <UIKit/UIKit.h>
#include <AudioToolbox/AudioToolbox.h>
#import <AVFoundation/AVAudioPlayer.h>
#import "AVFoundation/AVFoundation.h"
#import "DoorGuard_Util.h"
#import "CallViewController.h"
#import "HomeViewController.h"
#import <VBellCloud/VBellCloud.h>

@interface LinphoneAppDelegate : NSObject <UIApplicationDelegate,UIAlertViewDelegate>
{
@private UIBackgroundTaskIdentifier bgStartId;
    BOOL startedInBackground;
    AVAudioPlayer *playerRingTone;
    BOOL LoginFlag;
    VBell_Cloud* vbellCloud;
    NSString* incomming_device;
    
    HomeViewController* homeViewController;
    CallViewController* callViewController;
    NSMutableArray<NSMutableArray<NSMutableDictionary*>*>* deviceList;
}
@property (nonatomic, retain) UIAlertView* waitingIndicator;
@property (nonatomic, retain) NSString* configURL;
@property (nonatomic, strong) UIWindow* window;
@property (nonatomic) BOOL LoginFlag;
@property (retain, nonatomic) IBOutlet CustomNavigation* Navigation;
@property (nonatomic, retain) VBell_Cloud* vbellCloud;
+(LinphoneAppDelegate*)sharedInstance;
-(void)PlayRingTone;
-(void)StopRingTone;
-(void)UpdatePushNotificationID;
-(void)UpdateDeviceList:(NSMutableArray<NSMutableArray<NSMutableDictionary*>*>*) _deviceList;
-(NSMutableArray<NSMutableDictionary*>*)GetOwnDeviceList;
-(NSMutableArray<NSMutableDictionary*>*)GetViewDeviceList;
-(void)ShowBusyViewWithMessage:(NSString*)_message;
-(void)HideBusyView;
-(void)GoTo_HomeViewController;
@end

#endif
