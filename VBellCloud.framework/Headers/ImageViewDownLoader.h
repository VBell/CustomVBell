//
//  ImageViewDownloader.h
//  idoubs
//
//  Created by APPLE on 12/3/5.
//  Copyright (c) 2012年 Doubango Telecom. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>

@interface ImageViewDownLoader : UIImageView
{
    UIActivityIndicatorView *indicator;

    NSMutableData *receivedata;
    
    NSInteger index_tag;
}
@property (retain, nonatomic) UIActivityIndicatorView *indicator;
@property (retain, nonatomic) NSMutableData *receivedata;
@property (nonatomic) NSInteger index_tag;

- (BOOL)startImageDownload_big:(NSString *)picid;
- (BOOL)startImageDownload:(NSString *)picid;

@end
