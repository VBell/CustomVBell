//
//  ScanDP.h
//  linphone
//
//  Created by APPLE on 2014/9/10.
//
//

#import <Foundation/Foundation.h>

@class Zwave_Connect;

@protocol ScanDP <NSObject>

- (void) ScanDP_Result:(NSMutableArray *)ip_array;

@end

@interface ScanDP : NSObject

@property (nonatomic,retain) id<ScanDP> delegate;

- (id)initScanDPWithDelegate:(id<ScanDP>)_delegate;

- (void)StartScan;

@end
