//
//  BusyView.h
//  ChinaTrust
//
//  Created by Yehsam on 2010/1/27.
//  Copyright 2010 FoneStock. All rights reserved.
//

#ifndef _BUSY_VIEW_H_
#define _BUSY_VIEW_H_
#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>

@interface BusyView : UIView {
	CGRect      backgroundViewRect;
    UIColor     *strokeColor;
    UIColor     *rectColor;
    CGFloat     strokeWidth;
    CGFloat     cornerRadius;
	UILabel		*loadLabel;
    UIActivityIndicatorView *actView;
}
@property (nonatomic, readwrite) CGRect backgroundViewRect;
@property (nonatomic, retain) UIColor *strokeColor;
@property (nonatomic, retain) UIColor *rectColor;
@property CGFloat strokeWidth;
@property CGFloat cornerRadius;
@property (nonatomic, retain)  UILabel		*loadLabel;
@property (nonatomic, retain) UIActivityIndicatorView *actView;

- (id)initWithFrame:(CGRect)frame loadingViewRect:(CGRect)loadingViewRec;
- (void)setLoadText:(NSString*)text;

+(void)addBusyViewFromView:(UIView*)baseView message:(NSString*)message;
+(void)addBusyViewFromView:(UIView*)baseView;
+(void)addBusyViewFromViewAndSub:(UIView*)baseView;

+(void)removeBusyViewFromView:(UIView*)baseView;
+(void)updateBusyView:(UIView*)baseView;

@end

#endif
