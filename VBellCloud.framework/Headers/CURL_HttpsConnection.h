//
//  CURL_HttpsConnection.h
//  avaControl Remote
//
//  Created by David on 2015/6/20.
//  Copyright (c) 2015年 avaControl Remote. All rights reserved.
//

#ifndef CURL_HttpsConnection_h
#define CURL_HttpsConnection_h

#import <Foundation/Foundation.h>
#define CURL_CONNECTION_RETRY_MAX 1

@interface CURL_HttpsConnection : NSObject

- (id)initWithResponse_FinishSelector:(SEL)selector ErrSelector:(SEL)errselector Delegate:(id)_delegate;

- (id)initWithResponse_FinishSelector:(SEL)selector ErrSelector:(SEL)errselector Delegate:(id)_delegate Acc:(NSString *)_acc Pwd:(NSString *)_pwd;

- (void)setRequestFlag:(NSInteger)flag;

- (BOOL)Http_Connect_Post_URL:(NSString*)url PostData:(NSString *)post;
- (BOOL)Http_Connect_Post_URL:(NSString *)url PostData:(NSString *)post ReturnToMainThread:(BOOL)main_thread;
- (BOOL)Http_Connect_Get_URL:(NSString*)url;
- (BOOL)Http_Connect_Get_URL:(NSString *)url ReturnToMainThread:(BOOL)main_thread;

@end

@interface CURL_HttpsConnectionResponse : NSObject
{
    NSString* resCode;
    NSString* resMessage;
    NSInteger reqFlag;
}

@property(nonatomic,strong) NSString* resCode;
@property(nonatomic,strong) NSString* resMessage;
@property(nonatomic)        NSInteger reqFlag;

@end

#endif
