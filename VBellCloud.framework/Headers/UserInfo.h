//
//  ns.h
//  test api
//
//  Created by APPLE on 2014/7/9.
//  Copyright (c) 2014年 test api. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <sys/sysctl.h>

@interface UserInfo : NSObject

#pragma mark - GetForKey

+(NSString*)getStringForKey:(NSString*)key;
+(id)getObjectForKey:(NSString *)key;
+(NSArray *)getArrayForKey:(NSString *)key;
+(NSDictionary *)getDictionaryForKey:(NSString *)key;
+(NSData *)getDataForKey:(NSString *)key;
+(BOOL)getBooleanForKey:(NSString*)key;
+(NSInteger)getIntegerForKey:(NSString*)key;
+(double)getDoubleForKey:(NSString*)key;
+(float)getFloatForKey:(NSString*)key;

#pragma mark - SetForKey

+(BOOL)setString:(NSString*)value forKey:(NSString*)key;
+(BOOL)setObject:(id)value forKey:(NSString *)key;
+(BOOL)setArray:(NSArray*)value forKey:(NSString *)key;
+(BOOL)setDictionary:(NSDictionary*)value forKey:(NSString *)key;
+(BOOL)setData:(NSData*)value forKey:(NSString *)key;
+(BOOL)setBoolean:(BOOL)value forKey:(NSString*)key;
+(BOOL)setInteger:(NSInteger)value forKey:(NSString*)key;
+(BOOL)setDouble:(double)value forKey:(NSString *)key;
+(BOOL)setFloat:(float)value forKey:(NSString *)key;


+ (NSString *) platform;
+ (NSString *) platformString;
@end
