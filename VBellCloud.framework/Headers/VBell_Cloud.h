//
//  VBell_Cloud.h
//  VBell
//
//  Created by David on 2016/5/13.
//  Copyright (c) 2016年 VBell. All rights reserved.
//

#ifndef VBell_Cloud_h
#define VBell_Cloud_h

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>
#import "ScanDP.h"

extern NSString *const kRegisterResponse;
extern NSString *const kLoginResponse;
extern NSString *const kForgotPasswordResponse;
extern NSString *const kGetDeviceListResponse;
extern NSString *const kDeleteDeviceResponse;
extern NSString *const kGetDeviceStatusResponse;
extern NSString *const kSetDeviceStatusResponse;
extern NSString *const kAddDeviceResponse;
extern NSString *const kGetAlarmResponse;
extern NSString *const kSetAlarmResponse;
extern NSString *const kAddViewerResponse;
extern NSString *const kRemoveViewerResponse;
extern NSString *const kGetViewerResponse;
extern NSString *const kSetNetworkResponse;
extern NSString *const kGetNetworkResponse;
extern NSString *const kGetEventLogResponse;
extern NSString *const kUpdatePushIDResponse;

typedef enum _VBellCloud_CallUpdateEvent {
    //VBellCloud_CallUpdateEvent_IncomingReceived,
    //VBellCloud_CallUpdateEvent_OutgoingInit,
    //VBellCloud_CallUpdateEvent_Connected,
    //VBellCloud_CallUpdateEvent_Error,
    //VBellCloud_CallUpdateEvent_End,
    //VBellCloud_CallUpdateEvent_Answered,
    
    VBellCloud_CallUpdateEvent_Incoming,
    VBellCloud_CallUpdateEvent_Hangup,
    VBellCloud_CallUpdateEvent_Answered,
    VBellCloud_CallUpdateEvent_Connected,
    VBellCloud_CallUpdateEvent_Outgoing,
    VBellCloud_CallUpdateEvent_End,
    VBellCloud_CallUpdateEvent_Error
} VBellCloud_CallUpdateEvent;
extern NSString *const kCallUpdateEvent;

@interface VBell_Cloud : NSObject<ScanDP>
#pragma mark - Register Account
+ (BOOL)NSStringIsValidEmail:(NSString *)checkString;
- (BOOL)registerAccount:(NSString*)account withPassword:(NSString*)password withTag:(NSInteger)_tag returnToMainThread:(BOOL)mainThread;

#pragma mark - Login
- (BOOL)loginAccWith:(NSString*)account withPassword:(NSString*)password withTag:(NSInteger)_tag returnToMainThread:(BOOL)mainThread;

#pragma mark - Forgot Password
- (BOOL)forgotPasswordFor:(NSString*)account withTag:(NSInteger)_tag returnToMainThread:(BOOL)mainThread;

#pragma mark - Get Device List
- (BOOL)getDeviceListWithTag:(NSInteger)_tag checkError:(BOOL)needCheck returnToMainThread:(BOOL)mainThread;

#pragma mark - Delete Device
- (BOOL)deleteDevice:(NSString*)deviceID withTag:(NSInteger)_tag returnToMainThread:(BOOL)mainThread;

#pragma mark - Get Device Status
- (BOOL)getDeviceStatus:(NSString*)deviceID withTag:(NSInteger)_tag returnToMainThread:(BOOL)mainThread;

#pragma mark - Set Device Status
- (BOOL)setDeviceStatus:(NSString*)deviceID withName:(NSString*)name withState:(NSDictionary*)_deviceState withTag:(NSInteger)_tag returnToMainThread:(BOOL)mainThread;

#pragma mark - Add Device With Code
- (BOOL)addDeviceWithCode:(NSString*)deviceCode withTag:(NSInteger)_tag returnToMainThread:(BOOL)mainThread;

#pragma mark - Get Alarm State
- (BOOL)getAlarmWithTag:(NSInteger)_tag returnToMainThread:(BOOL)mainThread;

#pragma mark - Set Alarm State
- (BOOL)setAlarm:(BOOL)_state WithTag:(NSInteger)_tag returnToMainThread:(BOOL)mainThread;

#pragma mark - Add Viewer For Device
- (BOOL)addViewerForDevice:(NSString*)device_mac withViewer:(NSString*)new_viewer WithTag:(NSInteger)_tag returnToMainThread:(BOOL)mainThread;

#pragma mark - Remove viewer For Device
- (BOOL)removeViewerForDevice:(NSString*)device_mac withViewer:(NSString*)viewer WithTag:(NSInteger)_tag returnToMainThread:(BOOL)mainThread;

#pragma mark - Get Viewer For Device
- (BOOL)getViewerForDevice:(NSString*)device_mac WithTag:(NSInteger)_tag returnToMainThread:(BOOL)mainThread;

#pragma mark - Set Network For Device
- (BOOL)setNetworkForDeviceIP:(NSString*)ip withModel:(NSString*)_model withSSID:(NSString*)ssid withAmode:(NSString*)amode withEtype:(NSString*)etype withAkey:(NSString*)akey WithTag:(NSInteger)_tag returnToMainThread:(BOOL)mainThread;

#pragma mark - Get Network For Device
- (BOOL)getNetworkForDeviceIP:(NSString*)ip withModel:(NSString*)_model WithTag:(NSInteger)_tag returnToMainThread:(BOOL)mainThread;

#pragma mark - Update Push ID
- (BOOL)updatePushID:(NSString*)push_id WithTag:(NSInteger)_tag returnToMainThread:(BOOL)mainThread;

#pragma mark - Get event Log
- (BOOL)getEventLogWithTag:(NSInteger)_tag returnToMainThread:(BOOL)mainThread;

- (BOOL)Call:(NSString*)target_mac;
- (BOOL)AnswerCall;
- (BOOL)HangupCall:(BOOL)_forWallPad;
- (BOOL)DoorUnlock;
- (void)processRemoteNotification:(NSDictionary*)userInfo;
- (UIImage*)GetAccountQRCode;
- (UIImage*)GetSnapshotFromDevice:(NSString*)_mac withKey:(NSString*)_key;

- (void)ApplicationDidEnterBackground;
- (void)ApplicationWillResignActive;
- (void)ApplicationDidBecomeActive;
- (void)setNativeVideoWindowID:(void*)windowID;
@end

#endif
